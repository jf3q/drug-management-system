<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<%@ include file="common.jsp"%>
</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinnero.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pageo.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper" data-navbarbg="skin6" data-theme="light"
		data-layout="vertical" data-sidebartype="full"
		data-boxed-layout="full">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pageo.scss -->
		<!-- ============================================================== -->
		<%@ include file="leftmenu.jsp"%>
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->

			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->
				<!-- Row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-dname">添加&编辑采购信息</h4>

								<form   class="form-horizontal form-material" action="${ctx }/jsp/wreck/save" method="post" onsubmit="return onsub();">
									<input type="hidden" name="pageNo" value="${pageNo }">
									<input type="hidden" name="id"   value="${o.id }">
									<div class="form-group">
										<label class="col-md-12">药品：</label>
										<div class="col-md-12">
											<input type="hidden" name="drugId"  id="drugId" value="${o.drugId }">
											<input type="text"   name="medicineName" id="iname"  value="${o.medicineName }"  maxlength="30" required="required" readonly="readonly"  class="form-control form-control-line">
										</div>
									</div>


									<div class="form-group">
										<label class="col-md-12">数量：</label>
										<div class="col-md-12">
											<input type="text"   name="inboundQuantity" value="${empty o.inboundQuantity?1:o.inboundQuantity }" pattern="[0-9]+\.?[0-9]*" min=1   required="required"   class="form-control form-control-line">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-12">生产单位：</label>
										<div class="col-md-12">
											<input type="text" id="unit"  name="manufacturer" value="${o.manufacturer }" readonly="readonly"   required="required"   class="form-control form-control-line">
										</div>
									</div>


<c:if test="${staffU.roleType == 1 }">
									<div class="form-group">
										<label class="col-md-12">操作人员：</label>
										<div class="col-md-12">
											 <select name="operatorId"   class="form-control form-control-line"  >
	                                    	<c:forEach items="${sli }" var="t">
	                                    	<option value="${t.id }" >${t.userName}</option>
	                                    	</c:forEach>
	                                    </select>
										</div>
									</div>

	<div class="form-group">
		<label class="col-md-12">价格：</label>
		<div class="col-md-12">
			<input type="text" id="boundPrice"  name="inboundPrice" value="${o.inboundPrice }"  pattern="[0-9]+\.?[0-9]*" min=1  required="required"   class="form-control form-control-line">
		</div>
	</div>
</c:if>

<c:if test="${staffU.roleType == 2 }">

									<div class="form-group">
										<label class="col-md-12">病人：</label>
										<div class="col-md-12">
											<textarea  id="supplier_msg" readonly="readonly" name="supplier_msg"  maxlength="200" class="form-control form-control-line">${o.supplier_msg } </textarea>
										</div>
									</div>
</c:if>

									<div class="form-group">
										<label class="col-md-12">备注：</label>
										<div class="col-md-12">
											<textarea    name="inboundReason"   id="buy_note"   maxlength="200" class="form-control form-control-line">${o.inboundReason } </textarea>
										</div>
									</div>




									<div class="form-group">
										<div class="col-sm-12">
										 <p style="color:red;">${msg }</p>
											<button class="btn btn-success" type="submit">提交</button>
										</div>
									</div>
								</form>
							</div>

						</div>

					</div>

				</div>
				<!-- Row -->
				<!-- ============================================================== -->
				<!-- End PAge Content -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Right sidebar -->
				<!-- ============================================================== -->
				<!-- .right-sidebar -->
				<!-- ============================================================== -->
				<!-- End Right sidebar -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>



<!-- 药品模态框（Modal） -->
<div class="modal fade" id="shicaiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">选择需要购买的药品</h4>
            </div>

            <div class="modal-body">
             	<form class="form-inline" role="form">
				  <input type="text" class="form-control" id="shicai_code" placeholder="请输入编号" style="width:120px;">
				  <input type="text" class="form-control" id="shicai_iname" placeholder="请输入名称"  style="width:120px;" >
				  <button type="button" id="shicai_find" class="btn btn-success">查找</button>
				  </form>
				 <div style="clear: both;"></div>
				  <table class="table table-striped">
				  <thead>
				    <tr>
				      <th>编号</th>
				      <th>名称</th>
				      <th>库存</th>
				      <th>描述</th>
				    </tr>
				  </thead>
				  <tbody id="shicai_body">  </tbody>
				</table>
				<div id="shicai_page" style="text-align: center;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<!-- 药品模态框（Modal） -->


<!-- 病人模态框（Modal） -->
<div class="modal fade" id="supplierModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">选择病人</h4>
            </div>

            <div class="modal-body">
             	<form class="form-inline" role="form">
				  <input type="text" class="form-control" id="supplier_sname" placeholder="请输入名称">
				  <button type="button" id="supplier_find" class="btn btn-success">查找</button>
				  </form>
				 <div style="clear: both;"></div>
				  <table class="table table-striped">
				  <thead>
				    <tr>
				      <th>名称</th>
				      <th>性别</th>
				      <th>手机号</th>
				    </tr>
				  </thead>
				  <tbody id="supplier_body"> </tbody>
				</table>
				<div id="supplier_page" style="text-align: center;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<!-- 病人模态框（Modal） -->


</body>


 <script type="text/javascript">


  /*--获取药品--------------------------*/
  $('#iname').click(function(){
	  $('#shicaiModal').modal('show');
	  get_ingredients();
  })
  function  get_ingredients(pageNo){
	  if(pageNo==undefined)pageNo=1;
	  var code=$('#shicai_code').val();
	  var iname=$('#shicai_iname').val();
	  $.ajax({
			type:'post',
			url:"${ctx}/jsp/ingredients/getpage",
			data:{medicineName:iname,pageNo:pageNo,medicineCode:code},
			dataType:'json',
			success:function(data){
				var s="";
	               for(var i=0;i<data.list.length;i++){
	               	var one=data.list[i];
	               	s+='<tr data-idesc="'+one.usage+'" data-price="'+one.price+'" data-unit="'+one.manufacturer+'"  data-code="'+one.medicineCode+'"   data-id='+one.medicineId+' data-iname="'+one.medicineName+'">' +
							'<td>'+one.medicineCode+'</td>' +
							'<td>'+one.medicineName+'</td>' +
							'<td>'+one.stockQuantity+'</td>' +
							'<td>'+one.usage+'</td></tr>';
	               }
	               $('#shicai_body').html(s);
	               $('#shicai_page').html(print_page(data.pages,data.pageNum));
			}
		})
  }
  $('#shicai_page').on('click','a',function(){
	   	var pageNo=$(this).attr('data-p');
	   	get_ingredients(pageNo);
  });
  $('#shicai_find').click(function(){
	   	get_ingredients();
  });
  $('#shicai_body').on('click','tr',function(){
	  $('#iname').val($(this).attr('data-iname'));
	  $('#drugId').val($(this).attr('data-id'));
	  $('#buy_note').val($(this).attr('data-idesc'));
	  $('#boundPrice').val($(this).attr('data-price'));
	  $('#unit').val($(this).attr('data-unit'));
	  $('#shicaiModal').modal('hide');
  });



  /*---------病人--------------*/
    $('#supplier_msg').click(function(){
	  $('#supplierModal').modal('show');
	  get_supplier();
  })
  function  get_supplier(pageNo){
	  if(pageNo==undefined)pageNo=1;
	  var sname=$('#supplier_sname').val();
	  $.ajax({
			type:'post',
			url:"${ctx}/jsp/supplier/getpage",
			data:{userName:sname,pageNo:pageNo},
			dataType:'json',
			success:function(data){
				var s="";
	               for(var i=0;i<data.list.length;i++){
	               	var one=data.list[i];
					   s+='<tr data-s="'+one.userName+'('+one.gender+'&nbsp;'+one.phone+')"><td>'+one.userName+'</td><td>'+one.gender==1?'男':'女'+'</td><td>'+one.phone+'</td></tr>';
	               }
	               $('#supplier_body').html(s);
	               $('#supplier_page').html(print_page(data.pages,data.pageNum));
			}
		})
  }
  $('#supplier_page').on('click','a',function(){
	   	var pageNo=$(this).attr('data-p');
	   	get_supplier(pageNo);
  });
  $('#supplier_find').click(function(){
	   	get_supplier();
  });
  $('#supplier_body').on('click','tr',function(){
	  $('#supplier_msg').val($(this).attr('data-s'));
	  $('#supplierModal').modal('hide');
  });




  function print_page(pages,pageNo){
  	var totalPage=parseInt(pages);
  	if(totalPage==0)return "没有数据了";
  	var p="";
  	var page=parseInt(pageNo);
  	var cls=" ";
  	if(totalPage-1>0&&page-1>0){
  		p+='<a   data-p='+1+' class="btn btn-sm '+cls+'">首页</a>';
  		p+='<a  data-p='+(page-1)+' class="btn btn-sm '+cls+'">&laquo;</a>';
  	}
  	if(totalPage<=8){
  		for(var i=1;i<=totalPage;i++){
  			if(page==i)cls="btn-success";
  			else cls=" ";
  			p+='<a  data-p='+i+' class="btn btn-sm '+cls+'">'+i+'</a>';//&laquo;&raquo;
  		}
  	}
  	if(totalPage>8){
  		if(page-4>0){
  			if(page+4>=totalPage){
  				for(var i=page-3;i<=totalPage;i++){
  					if(page==i)cls="btn-success";
  					else cls=" ";
  					p+='<a   data-p='+i+' class="btn btn-sm '+cls+'">'+i+'</a>';//&laquo;&raquo;
  				}
  			}if(page+4<totalPage){
  				for(var i=page-4;i<=page+3;i++){
  					if(page==i)cls="btn-success";
  					else cls=" ";
  					p+='<a  data-p='+i+' class="btn btn-sm '+cls+'">'+i+'</a>';//&laquo;&raquo;
  				}
  			}

  		}if(page-4<=0){
  			for(var i=1;i<=8;i++){
  				if(page==i)cls="btn-success";
  				else cls=" ";
  				p+='<a   data-p='+i+' class="btn btn-sm '+cls+'">'+i+'</a>';//&laquo;&raquo;
  			}
  		}
  	}
  	if(totalPage>1&&page+1<totalPage){
  		p+='<a  data-p='+(page+1)+' class="btn btn-sm '+cls+'">&raquo;</a>';
  		p+='<a  data-p='+totalPage+' class="btn btn-sm '+cls+'">尾页</a>';
  	}
  	return p;
  }


  function onsub(){
	  var id=$('#drugId').val();
	  if(id.length==0){
		  alert('请选择需要购买的药品');
		  return false;
	  }
	  return true;
  }
 </script>


</html>
