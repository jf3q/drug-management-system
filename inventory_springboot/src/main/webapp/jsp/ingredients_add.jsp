<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<%@ include file="common.jsp"%>
</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinnero.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pageo.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper" data-navbarbg="skin6" data-theme="light"
		data-layout="vertical" data-sidebartype="full"
		data-boxed-layout="full">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pageo.scss -->
		<!-- ============================================================== -->
		<%@ include file="leftmenu.jsp"%>
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->

			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->
				<!-- Row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-dname">添加&编辑库存</h4>

								<form   class="form-horizontal form-material" action="${ctx }/jsp/ingredients/save" method="post" onsubmit="return onsub();">
									<input type="hidden" name="pageNo" value="${pageNo }">
									<input type="hidden" name="medicineId"   value="${o.medicineId }">
									<div class="form-group">
										<label class="col-md-12">药品名称：</label>
										<div class="col-md-12">
											<input type="text"   name="medicineName"  value="${o.medicineName }"  maxlength="30" required="required"   class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">价格：</label>
										<div class="col-md-12">
											<input type="text"   name="price"  value="${o.price }"  maxlength="30" required="required"   class="form-control form-control-line">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-12">剂型：</label>
										<div class="col-md-12">
											<input type="text"   name="dosageForm" value="${o.dosageForm }"   class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">规格：</label>
										<div class="col-md-12">
											<input type="text"   name="specification" value="${o.specification}"    class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">上市许可持有人：</label>
										<div class="col-md-12">
											<input type="text"   name="approvalHolder" value="${o.approvalHolder}"    class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">生产单位：</label>
										<div class="col-md-12">
											<input type="text"   name="manufacturer" value="${o.manufacturer}"    class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">批准文号：</label>
										<div class="col-md-12">
											<input type="text"   name="approvalNumber" value="${o.approvalNumber}"    class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">用途：</label>
										<div class="col-md-12">
											<input type="text"   name="usage" value="${o.usage}"    class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">药品编码：</label>
										<div class="col-md-12">
											<input type="text"   name="medicineCode" value="${o.medicineCode}"    class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">保质期：</label>
										<div class="col-md-12">
											<input type="text"   name="expirationDate" value="<fmt:formatDate value="${o.expirationDate}" pattern="yyyy-MM-dd" />"    class="form-control form-control-line">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-12">备注或者描述：</label>
										<div class="col-md-12">
											<textarea    name="usage"  maxlength="200" class="form-control form-control-line">${o.usage } </textarea>
										</div>
									</div>



									<div class="form-group">
										<div class="col-sm-12">
										 <p style="color:red;">${msg }</p>
											<button class="btn btn-success" type="submit">提交</button>


										</div>
									</div>
								</form>
							</div>

						</div>

					</div>

				</div>
				<!-- Row -->
				<!-- ============================================================== -->
				<!-- End PAge Content -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Right sidebar -->
				<!-- ============================================================== -->
				<!-- .right-sidebar -->
				<!-- ============================================================== -->
				<!-- End Right sidebar -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>

</body>


 <script type="text/javascript">
  $('#unit_choose button').click(function(){
	  if($(this).hasClass('btn-success')){
		  $(this).removeClass('btn-success').addClass('btn-default');
		  $('#unit').val('');
	  }else{
		  $('#unit_choose button').removeClass('btn-success').addClass('btn-default');
		  $(this).addClass('btn-success').removeClass('btn-default');
		  $('#unit').val( $(this).text() );
	  }

  })
 </script>


</html>
