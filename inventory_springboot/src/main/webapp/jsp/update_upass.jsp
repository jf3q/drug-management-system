<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<%@ include file="common.jsp"%>
</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinnero.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pageo.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper" data-navbarbg="skin6" data-theme="light"
		data-layout="vertical" data-sidebartype="full"
		data-boxed-layout="full">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pageo.scss -->
		<!-- ============================================================== -->
		<%@ include file="leftmenu.jsp"%>
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->

			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->
				<!-- Row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-dname">修改密码</h4>
								 
								<form   class="form-horizontal form-material" action="${ctx }/jsp/staff/updateUpass" method="post" onsubmit="return onsub();">
									 
									<div class="form-group">
										<label class="col-md-12">原密码：</label>
										<div class="col-md-12">
											<input type="password"   name="old_upass"  pattern="(\w){6,20}"   required="required"   class="form-control form-control-line"> 
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-12">新密码（6-12位）：</label>
										<div class="col-md-12">
											<input type="password"   name="new_upass"  pattern="(\w){6,20}"   required="required"   class="form-control form-control-line"> 
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-12">确认新密码（6-12位）：</label>
										<div class="col-md-12">
											<input type="password"   name="new_upass2"  pattern="(\w){6,20}"   required="required"   class="form-control form-control-line"> 
										</div>
									</div>
									 
									<div class="form-group">
										<div class="col-sm-12">
										 <p style="color:red;">${msg }</p>
											<button class="btn btn-success" type="submit">提交</button>
										 
										 
										</div>
									</div>
								</form>
							</div>

						</div>

					</div>

				</div>
				<!-- Row -->
				<!-- ============================================================== -->
				<!-- End PAge Content -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Right sidebar -->
				<!-- ============================================================== -->
				<!-- .right-sidebar -->
				<!-- ============================================================== -->
				<!-- End Right sidebar -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>

</body>

 
 <script type="text/javascript">
 function onsub(){
	 var s=$('#start_ts').val();
	 var e=$('#end_ts').val();
	 if(s>e){
		 alert('开始时间必须小于或等于结束时间');
		 return false;
	 }
	 return true;
 }
 
 </script>


</html>