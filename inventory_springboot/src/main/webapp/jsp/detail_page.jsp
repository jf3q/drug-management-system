<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
      <%@ include file="common.jsp"%>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
         <%@ include file="leftmenu.jsp"%>
       <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                            <div class="card-body">
                               <h4 class="card-dname">库存明细</h4>
                               <form id="search_form" class="form-inline m-t-30" action="${ctx }/jsp/detail/page" method="post">
	                                 <div class="form-group">
	                                    <input type="text" name="userName" value="${o.userName }" class="form-control"  placeholder="操作员名称">
	                                 </div>

	                                 <div class="form-group">
	                                    <input type="text" name="medicineName" value="${o.medicineName }" class="form-control"  placeholder="药品名称">
	                                 </div>

<%--	                                 <div class="form-group">--%>
<%--	                                    <select name="type_id"   class="form-control"  >--%>
<%--	                                    	<option value="">-分类-</option>--%>
<%--	                                    	<c:forEach items="${tli }" var="t">--%>
<%--	                                    	<option value="${t.id }" <c:if test="${t.id eq o.type_id }">selected="selected"</c:if> >${t.tname }</option>--%>
<%--	                                    	</c:forEach>--%>
<%--	                                    </select>--%>
<%--	                                </div>--%>

	                                <div class="form-group">
	                                	<button class="btn btn-success" type="submit">查找</button>
                                        <a href="${ctx }/jsp/receipt/toedit" class="btn btn-warning">添加</a>
	                           		<a href="javascript:;" data-href="${ctx }/jsp/excel/getDetail" class="btn btn-info import_excel">导出excel</a>
	                           		</div>
	                            </form>
                            </div>

                            <div class="table-responsive">
                            	<p style="color:red">${msg }</p>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">编号</th>
                                            <th scope="col">操作员名称</th>
                                            <th scope="col">药品名称</th>
                                            <th scope="col">生产单位</th>
                                            <th scope="col">数量</th>
                                            <th scope="col">单价</th>
                                            <th scope="col">总金额</th>
                                            <th scope="col">原因</th>
                                            <th scope="col">创建时间</th>
                                            <th scope="col">更新时间</th>
                                            <th scope="col">操作</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${pageInfo.list }" var="t">
                                         <tr>
                                              <td>${t.id }</td>
                                             <td>${t.userName }</td>
                                             <td>${t.medicineName }</td>
                                             <td>${t.manufacturer }</td>
                                              <td>${t.inboundQuantity }</td>
                                              <td>${t.inboundPrice }</td>
                                              <td>${t.totalAmount }</td>
                                              <td>${t.inboundReason }</td>
                                             <td><fmt:formatDate value="${t.createdAt}" pattern="yyyy-MM-dd" /></td>
                                             <td><fmt:formatDate value="${t.updatedAt}" pattern="yyyy-MM-dd" /></td>
                                               <td>
                                              <a href="javascript:;" data-href="${ctx }/jsp/detail/del?id=${t.id}&pageNo=${pageInfo.pageNum}" class="btn-danger btn-sm del" >删除</a>
                                              </td>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <form method="post" action="${ctx }/jsp/detail/page">
                                 <input type="hidden" name="medicineName" value="${o.medicineName }">
                                 <input type="hidden" name="userName" value="${o.userName }">
                                   <%@ include file="page.jsp"%>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

</body>
<script type="text/javascript">



$('.del').click(function(){

	if(confirm("确定删除？"))window.location=$(this).attr('data-href');
})

$('.import_excel').click(function(){
	 var s=$('#search_form').attr('action');
	 $('#search_form').attr('action',$(this).attr('data-href'));
	 $('#search_form').find('button[type=submit]').click();
	 $('#search_form').attr('action',s);
})
</script>
</html>
