<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.Period" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
      <%@ include file="common.jsp"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
         <%@ include file="leftmenu.jsp"%>
       <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                            <div class="card-body">
                                <h4 class="card-dname">员工管理</h4>
                               <form class="form-inline m-t-30" action="${ctx }/jsp/staff/page" method="post">
	                                <div class="form-group">
	                                    <input type="text" name="userName" value="${o.userName }" class="form-control"  placeholder="用户姓名">
	                                </div>
	                                <div class="form-group">
	                                    <select   name="gender"   class="form-control form-control-line">
											<option  value="0"   >-性别-</option>
											<option  value="1"  <c:if test="${o.gender == 1 }">selected="selected"</c:if> >男</option>
											<option  value="2"  <c:if test="${o.gender == 2 }">selected="selected"</c:if> >女</option>
										</select>
	                                </div>
	                                <div class="form-group">
	                                    <select   name="roleType"   class="form-control form-control-line">
											<option  value="0"   >-类型-</option>
											<option  value="1"  <c:if test="${o.roleType == 1 }">selected="selected"</c:if> >管理员</option>
											<option  value="2"  <c:if test="${o.roleType == 2 }">selected="selected"</c:if> >医生</option>
										</select>
	                                </div>


	                                <div class="form-group">
	                                	<button class="btn btn-success" type="submit">查找</button>
	                                	<a href="${ctx }/jsp/staff/toedit" class="btn btn-warning">添加</a>
	                           		</div>
	                            </form>
                            </div>

                            <div class="table-responsive">
                            	<p style="color:red">${msg }</p>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">用户名</th>
                                            <th scope="col">姓名</th>
                                            <th scope="col">性别</th>
                                            <th scope="col">出生日期</th>
                                            <th scope="col">创建时间</th>
                                            <th scope="col">操作</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${pageInfo.list }" var="t">
                                         <tr>
                                              <td>${t.account }</td>
                                              <td>${t.userName }</td>
                                              <td>${t.gender ==1?'男':'女' }</td>
                                              <td><fmt:formatDate value="${t.birthdate}" pattern="yyyy-MM-dd" /></td>
                                              <td><fmt:formatDate value="${t.createdAt}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                                              <td>
                                              	<a href="${ctx }/jsp/staff/toedit?id=${t.id}&pageNo=${pageInfo.pageNum}" class="btn-primary btn-sm">编辑</a>
                                                <a href="${ctx }/jsp/staff/initUpass?id=${t.id}&pageNo=${pageInfo.pageNum}" class="btn-info btn-sm">初始化密码</a>
			                                  	 <a href="javascript:;" data-href="${ctx }/jsp/staff/del?id=${t.id}&roleType=${t.roleType}&pageNo=${pageInfo.pageNum}" class="btn-danger btn-sm del" >删除</a>
                                              </td>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <form method="post" action="${ctx }/jsp/staff/page">
                                 <input type="hidden" name="userName" value="${o.userName }">
                                 <input type="hidden" name="gender" value="${o.gender }">
                                 <input type="hidden" name="roleType" value="${o.roleType }">
                                   <%@ include file="page.jsp"%>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

</body>
<script type="text/javascript">

$('.nouse').click(function(){
	var s=prompt('请输入禁用原因');
	if(s)window.location=$(this).attr('data-href')+"&msg="+s;
})

$('.del').click(function(){

	if(confirm("确定删除？"))window.location=$(this).attr('data-href');
})
</script>
</html>
