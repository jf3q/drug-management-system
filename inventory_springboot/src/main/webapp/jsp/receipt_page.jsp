<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
      <%@ include file="common.jsp"%>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
         <%@ include file="leftmenu.jsp"%>
       <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                            <div class="card-body">
                                <h4 class="card-dname">采购管理</h4>
                               <form class="form-inline m-t-30" action="${ctx }/jsp/receipt/page" method="post"  id="search_form">
	                                 <div class="form-group">
	                                    <input type="text" name="code" value="${o.code }" class="form-control"  placeholder="食材编号" style="width: 120px;">
	                                </div>
	                                 <div class="form-group">
	                                    <input type="text" name="iname" value="${o.iname }" class="form-control"  placeholder="名称" style="width: 120px;">
	                                </div>
	                                 <div class="form-group">
	                                    <input type="text" name="realname" value="${o.realname }" class="form-control"  placeholder="采购员姓名" style="width: 120px;">
	                                 </div>
	                                 <div class="form-group">
	                                    <select name="type_id"   class="form-control"  >
	                                    	<option value="">-分类-</option>
	                                    	<c:forEach items="${tli }" var="t">
	                                    	<option value="${t.id }" <c:if test="${t.id eq o.type_id }">selected="selected"</c:if> >${t.tname }</option>
	                                    	</c:forEach>
	                                    </select>
	                                </div>
	                                <div class="form-group">
	                                    <select name="status"   class="form-control"  >
	                                    	<option value="">-状态-</option>
	                                    	<option value="0" <c:if test="${o.status=='0' }">selected="selected"</c:if> >代采购</option>
	                                    	<option value="1" <c:if test="${o.status=='1' }">selected="selected"</c:if> >代入库</option>
	                                    	<option value="2" <c:if test="${o.status=='2' }">selected="selected"</c:if> >已入库</option>
	                                    </select>
	                                </div>
	                               
	                                <div class="form-group">
	                                	<button class="btn btn-success" type="submit">查找</button>
	                                	 <c:if test="${staffU.role eq 'admin' }"><a href="${ctx }/jsp/receipt/toedit" class="btn btn-warning">添加采购单</a></c:if>
	                                	 <a href="javascript:;" data-href="${ctx }/jsp/excel/getReceipt" class="btn btn-info import_excel">导出excel</a>
	                           		</div>
	                            </form>
                            </div>
                            
                            <div class="table-responsive">
                            	<p style="color:red">${msg }</p>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">采购单</th>
                                            <th scope="col">创建时间</th>
                                            <th scope="col">状态</th>
                                            <th scope="col">操作</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${pageInfo.list }" var="t">
                                         <tr>
                                              <td>编号：${t.code } 
                                              <br>${t.iname }(分类：${t.tname })
                                              <br>需采购数量：${t.buy_num } ${t.unit }
                                              <br>采购员：${t.realname }
                                              <br>采购日期：${t.buy_ts }
                                               <br>供货商：${t.supplier_msg }
                                               <br>采买备注：${t.buy_note }
                                              <c:if test="${t.status !='0' }">
                                              <span style="color:blue">
                                              	 <br>实际采买数量：${t.buyed_num } ${t.unit }
	                                               <br>单价：${t.price }
	                                               <br>总价：${t.total_money }
	                                               <br>采购时间：${t.buyed_ts }
	                                               <br>采购员备注：${t.buyed_note }</span>
                                              </c:if>
	                                             
                                               <c:if test="${t.status =='2' }">
                                               <span style="color:red">
	                                               <br>折损数量：${t.wreck } ${t.unit }
	                                               <br>折损备注：${t.wreck_desc }</span>
	                                               <br>实际入库数量：${t.storage_num } ${t.unit }</span>
                                               </c:if>
	                                              
                                              </td> 
                                              <td>${t.cts }</td> 
                                              <td>
                                              <c:if test="${t.status=='0' }">待采购</c:if>
                                              <c:if test="${t.status=='1' }">待入库<br>采购员采购时间：${t.buyed_ts }
	                                               <br>采购员备注：${t.buyed_note } </c:if>
                                              <c:if test="${t.status=='2' }">已入库<br>入库时间：${t.storage_ts }
                                               	<br>入库备注：${t.storage_note }</c:if>
                                              
                                              </td> 
                                              
                                               
                                              <td>
                                              
                                              <c:if test="${staffU.role eq 'admin' }">
                                              			<c:if test="${t.status!='2'}">
		                                              	<a href="${ctx }/jsp/receipt/toedit?id=${t.id}&pageNo=${pageInfo.pageNum}" class="btn-primary btn-sm">编辑</a>
		                                               </c:if>
		                                               <c:if test="${t.status=='1'}">
		                                              <a href="javascript:;" 
		                                               data-href="${ctx }/jsp/receipt/putInStorage?id=${t.id}&pageNo=${pageInfo.pageNum}" 
		                                               data-iname="${t.iname }"
                                              	 	 	data-unit="${t.unit }"
                                              	 	 	data-buy_num="${t.buy_num }"
                                              	 	 	data-buyed_num="${t.buyed_num }"
                                              	 	 	data-price="${t.price }"
                                              	 	 	data-total_money="${t.total_money }"
                                              	 	 	data-buyed_note="${t.buyed_note}"
                                              	 	 	data-wreck="${t.wreck }"
                                              	 	 	data-wreck_desc="${t.wreck_desc }"
                                              	 	 	data-storage_note="${t.storage_note }"
                                              	 	 	data-storage_num="${t.storage_num }"
		                                               class="btn-info btn-sm putInStorage" >入库</a>
		    											</c:if>
		                                               <c:if test="${t.status=='0' }">
		                                              <a href="javascript:;" data-href="${ctx }/jsp/receipt/del?id=${t.id}&pageNo=${pageInfo.pageNum}" class="btn-danger btn-sm del" >删除</a>
		    											</c:if>
    											
    											</c:if>
                                              	 <c:if test="${staffU.role eq 'buyer' }">
                                              	 	<c:if test="${t.status=='0' }">
                                              	 	 	<a href="javascript:;" 
                                              	 	 	data-href="${ctx }/jsp/receipt/buyed?id=${t.id}&pageNo=${pageInfo.pageNum}" 
                                              	 	 	data-iname="${t.iname }"
                                              	 	 	data-unit="${t.unit }"
                                              	 	 	data-buy_num="${t.buy_num }"
                                              	 	 	class="btn-info btn-sm buyed">采购完成</a>
                                              	 	</c:if>
                                              	 
                                              	 </c:if>                               
                                              </td> 
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                
                                <form method="post" action="${ctx }/jsp/receipt/page">
                                 <input type="hidden" name="iname" value="${o.iname }">
                                 <input type="hidden" name="type_id" value="${o.type_id }">
                                 <input type="hidden" name="code" value="${o.code }">
                                    <input type="hidden" name="realname" value="${o.realname }">
                                   <%@ include file="page.jsp"%>
                                </form>
                            </div>
                        </div>
                    	
                    </div> 
                    
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

</body>


<!-- 提交采买信息模态框（Modal） -->
<div class="modal fade" id="buyedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">提交采买信息</h4>
            </div>
            
             	<form id="buyed_form" role="form" action="" method="post">
            <div class="modal-body">
             		 <table class="table table-bordered">
             		 <tr><td>食材：</td><td id="buyed_iname"></td></tr>
             		 <tr><td>需采买数量：</td><td id="buy_num_"></td></tr>
             		 <tr><td>单价：</td><td><input type="text" id="price" name="price" pattern="[0-9]+\.?[0-9]*" min=1 class="form-control" required="required"  style="width:100px;float:left;"    placeholder="单价">元</td></tr>
             		 <tr><td>实际采买数量：</td><td><input type="text" id="buyed_num" name="buyed_num" pattern="[0-9]+\.?[0-9]*" min=1 class="form-control" required="required"  style="width:100px;float:left;"    placeholder="数量"><span class="buyed_unit"></span></td></tr>
             		 <tr><td>总价：</td><td><input type="text" id="total_money" name="total_money" pattern="[0-9]+\.?[0-9]*" min=1 class="form-control" required="required"  style="width:100px;float:left;"    placeholder="总价">元</td></tr>
             		 <tr><td colspan="2"><textarea  name="buyed_note" id="buyed_note"  class="form-control"   maxlength="200"  placeholder="采购员的备注或其他说明"></textarea></td></tr>
             		 
             		 <tr id="storage_num_tr"><td>实际入库数量：</td><td><input type="text" id="storage_num" name="storage_num" value="0" pattern="[0-9]+\.?[0-9]*" min=0 class="form-control"    style="width:100px;float:left;"    placeholder="入库数量"><span class="buyed_unit"></span></td></tr>
             		 <tr id="wreck_tr"><td>损耗数量：</td><td><input type="text" id="wreck" name="wreck" value="0" pattern="[0-9]+\.?[0-9]*" min=0 class="form-control"    style="width:100px;float:left;"    placeholder="损耗数量"><span class="buyed_unit"></span></td></tr>
             		 <tr id="wreck_desc_tr" ><td colspan="2"><textarea id="wreck_desc"  name="wreck_desc"  class="form-control"   maxlength="200"  placeholder="消耗备注或其他说明"></textarea></td></tr>
             		 <tr id="storage_note_tr"><td colspan="2"><textarea id="storage_note"  name="storage_note"  class="form-control"   maxlength="200"  placeholder="入库说明或者备注"></textarea></td></tr>
             		 </table>
				</table>
				<div id="supplier_page" style="text-align: center;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="submit" class="btn btn-success">完成采购</button>
            </div>
				  </form>
           
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<!-- 提交采买信息模态框（Modal） -->



<script type="text/javascript">

 

$('.del').click(function(){
	 
	if(confirm("确定删除？"))window.location=$(this).attr('data-href');
})
$('.buyed').click(function(){
	 $('#buyedModal').modal('show');
	 $('#buyed_iname').text($(this).attr('data-iname'));
	 $('#buy_num_').text($(this).attr('data-buy_num')+$(this).attr('data-unit'));
	 $('.buyed_unit').text($(this).attr('data-unit'));
	$('#buyed_form').attr('action', $(this).attr('data-href')) ;
	
	$('#storage_num_tr').hide();
	$('#wreck_tr').hide();
	$('#wreck_desc_tr').hide();
	$('#storage_note_tr').hide();
	$('#storage_num').val('');
	$('#wreck').val('');
	$('#wreck_desc').val('');
	$('#storage_note').val('');
})

$('#price,#buyed_num').change(function(){
	var p=$('#price').val();
	var n=$('#buyed_num').val();
	if(isSZ(p)&&isSZ(n))$('#total_money').val(p*n);
	else $('#total_money').val('');
})
$('#storage_num').change(function(){
	var s=$('#storage_num').val();
	var b=$('#buyed_num').val();
	if(b-s<0){
		alert('实际入库数量必须小于实际购买数量');
		$('#wreck').val('');
	}
	if(isSZ(s)&&isSZ(b))$('#wreck').val(b-s);
	else $('#wreck').val('');
})
 
 function isSZ(s){  
		var patrn=/^[0-9]+\.?[0-9]*$/;  
		if (!patrn.exec(s)) return false
		return true
	}
	/*--------入库---------*/
	$('.putInStorage').click(function(){
		$('#buyedModal').modal('show');
		$('#buyed_form').attr('action', $(this).attr('data-href')) ;
		
		$('#storage_num_tr').show();
		$('#wreck_tr').show();
		$('#wreck_desc_tr').show();
		$('#storage_note_tr').show();
		
		$('#storage_num').val($(this).attr('data-storage_num'));
		$('#wreck').val($(this).attr('data-wreck'));
		$('#wreck_desc').val($(this).attr('data-wreck_desc'));
		$('#storage_note').val($(this).attr('data-storage_note'));
		
		
		
		$('#buyed_iname').text($(this).attr('data-iname'));
		 $('#buy_num_').text($(this).attr('data-buy_num')+$(this).attr('data-unit'));
		 $('#buyed_num').val($(this).attr('data-buyed_num')); 
		 $('#buyed_desc').val($(this).attr('data-buyed_desc')); 
		 $('#price').val($(this).attr('data-price')); 
		 $('#total_money').val($(this).attr('data-total_money')); 
		 $('#buyed_note').val($(this).attr('data-buyed_note')); 
		 $('#storage_note').val($(this).attr('data-storage_note')); 
		 $('.buyed_unit').text($(this).attr('data-unit'));
	})
	
	
	
$('.import_excel').click(function(){
	 var s=$('#search_form').attr('action');
	 $('#search_form').attr('action',$(this).attr('data-href'));
	 $('#search_form').find('button[type=submit]').click();
	 $('#search_form').attr('action',s);
})
</script>
</html>