<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<%@ include file="common.jsp"%>
</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinnero.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pageo.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper" data-navbarbg="skin6" data-theme="light"
		data-layout="vertical" data-sidebartype="full"
		data-boxed-layout="full">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pageo.scss -->
		<!-- ============================================================== -->
		<%@ include file="leftmenu.jsp"%>
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->

			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->
				<!-- Row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">


								<div class="panel panel-info">
									<div class="panel-heading">
										<h3 class="panel-title" style="text-align: center;">个人基本信息</h3>
									</div>
									<div class="panel-body">
										<table class="table table-bordered" style="width:60%;margin-left: 20%;">

											<tbody>
												<tr>
													<td>用户名</td>
													<td>${o.account }</td>
													<td>姓名</td>
													<td>${o.userName }</td>
												</tr>
												<tr>
													<td>性别</td>
													<td>${o.gender==1?'男':'女' }</td>
													<td>出生日期</td>
													<td><fmt:formatDate value="${o.birthdate}" pattern="yyyy-MM-dd" /></td>
												</tr>
												<tr>
													<td>角色</td>
													<td>

														${o.roleType == 1?'管理员':
														 o.roleType == 2?'医生':'病人'}

													</td>
													<c:if test="${o.roleType==3}">
														<td>创建时间</td>
														<td><fmt:formatDate value="${o.createdAt}" pattern="yyyy-MM-dd" /></td>
													</c:if>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</div>

						</div>

					</div>

				</div>
				<!-- Row -->
				<!-- ============================================================== -->
				<!-- End PAge Content -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Right sidebar -->
				<!-- ============================================================== -->
				<!-- .right-sidebar -->
				<!-- ============================================================== -->
				<!-- End Right sidebar -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>

</body>


<script type="text/javascript">

</script>


</html>
