<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<c:if test="${empty staffU }">

<script>
alert('请登录账号');
window.location="${ctx}/jsp/staff/tologin";

</script>

</c:if>
 <header class="topbar" data-navbarbg="skin6"  style="position: fixed;width:100%;">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <a href="javscript:;" class="logo">


                            <span class="logo-text">
                                后台管理系统
                            </span>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box">
                             <a href="#" target="_blank"></a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">你好， ${staffU.userName }  </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <a class="dropdown-item" href="${ctx }/jsp/staff/toUpdateUpass"><i class="mdi mdi-key-change"></i> 修改密码</a>
                           		  <a class="dropdown-item" href="${ctx }/jsp/staff/logout"><i class="mdi mdi-power"></i> 退出账号</a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5" style="position: fixed;">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                	 <ul id="sidebarnav">
                	<c:if test="${staffU.roleType == 1 }">

                   		<li class="sidebar-item">
                            <a href="${ctx }/jsp/staff/page"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-account-box"></i>
                                <span class="hide-menu">员工管理</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="${ctx }/jsp/supplier/page"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi  mdi-codepen"></i>
                                <span class="hide-menu">病人管理</span>
                            </a>
                        </li>

                         <li class="sidebar-item">
                            <a href="${ctx }/jsp/ingredients/page"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-content-save-settings"></i>
                                <span class="hide-menu">药品管理</span>
                            </a>
                        </li>
                         <li class="sidebar-item">
                            <a href="${ctx }/jsp/wreck/page"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-map"></i>
                                <span class="hide-menu">出库/待出库管理</span>
                            </a>
                        </li>
                         <li class="sidebar-item">
                            <a href="${ctx }/jsp/detail/page"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-comment"></i>
                                <span class="hide-menu">入库管理</span>
                            </a>
                        </li>



                    </c:if>
                   <c:if test="${staffU.roleType == 2 }">
                   		<li class="sidebar-item">
                            <a href="${ctx }/jsp/receipt/page"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-library"></i>
                                <span class="hide-menu">我的采购单</span>
                            </a>
                        </li>

                   </c:if>


                        <li class="sidebar-item">
                            <a href="${ctx }/jsp/staff/info"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-account"></i>
                                <span class="hide-menu">查看基本信息</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="${ctx }/jsp/staff/toUpdateUpass"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-key-change"></i>
                                <span class="hide-menu">修改密码</span>
                            </a>
                        </li>
                         <li class="sidebar-item">
                            <a href="${ctx }/jsp/staff/logout"  class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false">
                                <i class="mdi mdi-power" ></i>
                                <span class="hide-menu">退出系统</span>
                            </a>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <div style="height: 60px;">   </div>

