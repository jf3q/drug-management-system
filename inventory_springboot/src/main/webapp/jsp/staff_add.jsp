<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<%@ include file="common.jsp"%>
</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinnero.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pageo.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper" data-navbarbg="skin6" data-theme="light"
		data-layout="vertical" data-sidebartype="full"
		data-boxed-layout="full">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pageo.scss -->
		<!-- ============================================================== -->
		<%@ include file="leftmenu.jsp"%>
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->

			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->
				<!-- Row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-dname">添加&编辑员工</h4>

								<form   class="form-horizontal form-material" action="${ctx }/jsp/staff/save" method="post" onsubmit="return onsub();">
									<input type="hidden" name="pageNo" value="${pageNo }">
									<input type="hidden" name="id"  id="id" value="${o.id}">
									<div class="form-group">
										<label class="col-md-12">用户名/账号(用于登录,只能输入5-20个以字母开头、可带数字、“_”、“.”，初始密码：123456)：</label>
										<div class="col-md-12">
											<input type="text"   name="account" id="uname" data-c=-1 value="${o.account }"  <c:if test="${not empty o.id }">readonly="readonly"</c:if> pattern="[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}"   required="required"   class="form-control form-control-line">
										</div>
										<div class="col-md-12" id="checkUname_tip"></div>
									</div>

									<div class="form-group">
										<label class="col-md-12">姓名：</label>
										<div class="col-md-12">
											<input type="text"   name="userName" value="${o.userName }"    required="required" maxlength="10"  class="form-control form-control-line">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-12">性别：</label>
										<div class="col-md-12">
											<input type="radio"   name="gender" value="1"    required="required"  <c:if test="${o.gender ==1 }">checked="checked"</c:if>  >男
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio"   name="gender" value="2"    required="required"  <c:if test="${o.gender ==2 }">checked="checked"</c:if>  >女
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">e-mail：</label>
										<div class="col-md-12">
											<input type="text"   name="email" value="${o.email }"    required="required"  class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">电话：</label>
										<div class="col-md-12">
											<input type="text"   name="phone" value="${o.phone }"  pattern="[1][3,4,5,7,8,9][0-9]{9}"   required="required"  class="form-control form-control-line">
										</div>
									</div>


									<div class="form-group">
										<label class="col-md-12">出生日期：</label>
										<div class="col-md-12">
											<input type="date"   name="birthdate" value="<fmt:formatDate value="${o.birthdate}" pattern="yyyy-MM-dd" />"    required="required"  class="form-control form-control-line">
										</div>
									</div>


									 <div class="form-group">
										<label class="col-md-12">角色：</label>
										<div class="col-md-12">
											<select   name="roleType"    required="required"  class="form-control form-control-line">
												<option  value="1"  <c:if test="${o.roleType == 1 }">selected="selected"</c:if> >管理员</option>
												<option  value="2"  <c:if test="${o.roleType == 2 }">selected="selected"</c:if> >医生</option>
											</select>
										</div>
									</div>


									<div class="form-group">
										<div class="col-sm-12">
										 <p style="color:red;">${msg }</p>
											<button class="btn btn-success" type="submit">提交</button>


										</div>
									</div>
								</form>
							</div>

						</div>

					</div>

				</div>
				<!-- Row -->
				<!-- ============================================================== -->
				<!-- End PAge Content -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Right sidebar -->
				<!-- ============================================================== -->
				<!-- .right-sidebar -->
				<!-- ============================================================== -->
				<!-- End Right sidebar -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>

</body>


 <script type="text/javascript">

 $('#uname').change(function(){
	 checkUname();
 });

 function isRegisterUserName(s){
		var patrn=/^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}$/;
		if (!patrn.exec(s)) return false
		return true
	}

 function checkUname(){
		var id=$.trim($('#id').val());
		if(id.length>0){
			return;
		}
		var n=$('#uname').val();
		if(isRegisterUserName(n)==false){
			alert('用户名格式不对');
			return;
		}
		$.ajax({
			type:'post',
			url:"${ctx}/jsp/staff/checkUname",
			data:{uname:n},
			dataType:'json',
			success:function(data){
				if(data.status==1){
					$('#uname').attr('data-c',1);
					$('#checkUname_tip').html("&nbsp;&radic;"+data.msg).css({"color":"green"}).attr('data-c',"1");
					return true;
				}else{
					$('#uname').attr('data-c',0);
					$('#checkUname_tip').html("&nbsp;&Chi;"+data.msg).css({"color":"red"}).attr('data-c',"0");
					return false;
				}
			}
		})
	}


 function onsub(){
	  var id=$.trim($('#id').val());
		if(id.length>0){
			return true;
		}
		var c=$('#uname').attr('data-c');
		if(c==-1){
			checkUname();
			alert('该用户名已被使用');
			return false;
		}
		if(c==0){
			alert('该用户名已被使用');
			return false;
		}



		return true;
	}

 </script>


</html>
