<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
      <%@ include file="common.jsp"%>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
         <%@ include file="leftmenu.jsp"%>
       <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                            <div class="card-body">
                                <h4 class="card-dname">库存管理</h4>
                               <form class="form-inline m-t-30" action="${ctx }/jsp/ingredients/page" method="post" id="search_form">
	                                 <div class="form-group">
                                         <input type="text" name="medicineCode" value="${o.medicineCode }" class="form-control"  placeholder="药品编码">
	                                 </div>

	                                 <div class="form-group">
                                         <input type="text" name="medicineName" value="${o.medicineName }" class="form-control"  placeholder="药品名称">
	                                 </div>

	                                <div class="form-group">
	                                	<button class="btn btn-success" type="submit">查找</button>
	                                	<a href="${ctx }/jsp/ingredients/toedit" class="btn btn-warning">添加</a>
	                                	<a href="javascript:;" data-href="${ctx }/jsp/excel/getIngredients" class="btn btn-info import_excel">导出excel</a>
	                           		</div>
	                            </form>
                            </div>

                            <div class="table-responsive">
                            	<p style="color:red">${msg }</p>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">序号</th>
                                            <th scope="col">药品编码</th>
                                            <th scope="col">药品名称</th>
                                            <th scope="col">价格</th>
                                            <th scope="col">剂型</th>
                                            <th scope="col">规格</th>
                                            <th scope="col">生产工厂</th>
                                            <th scope="col">批准文号</th>
                                            <th scope="col">备注</th>
                                            <th scope="col">保质期</th>
                                            <th scope="col">库存数量</th>
                                            <th scope="col">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${pageInfo.list }" varStatus="i" var="t">
                                         <tr>
                                             <td>${i.index +1 }</td>
                                             <td>${t.medicineCode}</td>
                                             <td>${t.medicineName }</td>
                                             <td>${t.price}</td>
                                             <td>${t.dosageForm}</td>
                                             <td>${t.specification}</td>
                                             <td>${t.manufacturer}</td>
                                             <td>${t.approvalNumber}</td>
                                             <td>${t.usage}</td>
                                             <td>
                                                 <fmt:formatDate value="${t.expirationDate}" pattern="yyyy-MM-dd" />
                                             </td>
                                             <td>${t.stockQuantity}</td>
                                              <td>
                                                  <a href="${ctx }/jsp/ingredients/toedit?medicineId=${t.medicineId}&pageNo=${pageInfo.pageNum}" class="btn-primary btn-sm">编辑</a>
                                                  <a href="javascript:;" data-href="${ctx }/jsp/ingredients/del?medicineId=${t.medicineId}&pageNo=${pageInfo.pageNum}" class="btn-danger btn-sm del" >删除</a>
                                              </td>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <form method="post" action="${ctx }/jsp/ingredients/page">
                                    <input type="hidden" name="medicineName" value="${o.medicineName }">
                                    <input type="hidden" name="medicineCode" value="${o.medicineCode }">

                                   <%@ include file="page.jsp"%>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

</body>
<script type="text/javascript">



$('.del').click(function(){

	if(confirm("确定删除？"))window.location=$(this).attr('data-href');
})



$('.import_excel').click(function(){
	 var s=$('#search_form').attr('action');
	 $('#search_form').attr('action',$(this).attr('data-href'));
	 $('#search_form').find('button[type=submit]').click();
	 $('#search_form').attr('action',s);
})
</script>
</html>
