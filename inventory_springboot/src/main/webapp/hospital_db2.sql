/*
 Navicat Premium Data Transfer

 Source Server         : MySql
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : hospital_db2

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 29/03/2024 18:56:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员的唯一标识符，自动递增',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '电子邮件地址，不能为空且要求唯一',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '联系电话，不能为空',
  `createdAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '账户创建时间戳，默认为当前时间',
  `updatedAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '账户最后修改时间戳，默认为当前时间，并在更新时自动更新',
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` int(11) NOT NULL COMMENT '性别',
  `birthdate` date NOT NULL COMMENT '出生日期',
  `userId` int(11) NOT NULL COMMENT '用户表外键',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 'admin@163.com', '13272000000', '2024-03-27 19:23:46', '2024-03-27 19:23:56', '管理员', 1, '1990-06-27', 1);

-- ----------------------------
-- Table structure for diagnosis_record
-- ----------------------------
DROP TABLE IF EXISTS `diagnosis_record`;
CREATE TABLE `diagnosis_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '诊断记录的唯一标识符，自动递增',
  `templateId` int(11) NOT NULL COMMENT '关联到诊断模板的外键',
  `patientId` int(11) NOT NULL COMMENT '关联到患者的外键',
  `doctorId` int(11) NOT NULL COMMENT '关联到医生的外键',
  `diagnosisDetails` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详细的诊断内容',
  `createdAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '诊断记录创建时间戳，默认为当前时间',
  `updatedAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '诊断记录最后修改时间戳，默认为当前时间，并在更新时自动更新',
  `outId` int(11) NOT NULL COMMENT '关联到出库，默认为-1（表示未开药）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of diagnosis_record
-- ----------------------------
INSERT INTO `diagnosis_record` VALUES (1, 1, 1, 2, '患者出现感冒典型症状，初步诊断为普通感冒', '2024-03-26 16:43:29', '2024-03-28 20:07:19', 1);
INSERT INTO `diagnosis_record` VALUES (2, 2, 2, 2, '患者血压持续偏高，诊断为高血压Ⅰ级', '2024-03-26 16:43:29', '2024-03-27 08:32:07', 3);

-- ----------------------------
-- Table structure for diagnosis_template
-- ----------------------------
DROP TABLE IF EXISTS `diagnosis_template`;
CREATE TABLE `diagnosis_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '诊断模板的唯一标识符',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诊断模板的名称',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '诊断模板的描述',
  `createdAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '诊断模板创建的时间戳',
  `updatedAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '诊断模板更新的时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of diagnosis_template
-- ----------------------------
INSERT INTO `diagnosis_template` VALUES (1, '普通感冒模板', '适用于常见感冒症状的诊断与治疗方案', '2024-03-26 16:43:11', '2024-03-26 16:43:11');
INSERT INTO `diagnosis_template` VALUES (2, '高血压病模板', '针对高血压病人的常规诊断及治疗建议', '2024-03-26 16:43:11', '2024-03-26 16:43:11');

-- ----------------------------
-- Table structure for doctors
-- ----------------------------
DROP TABLE IF EXISTS `doctors`;
CREATE TABLE `doctors`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员/医生的唯一标识符，自动递增',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '电子邮件地址，不能为空且要求唯一',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '联系电话，不能为空',
  `createdAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '账户创建时间戳，默认为当前时间',
  `updatedAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '账户最后修改时间戳，默认为当前时间，并在更新时自动更新',
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` int(11) NOT NULL COMMENT '性别',
  `birthdate` date NOT NULL COMMENT '出生日期',
  `userId` int(11) NULL DEFAULT NULL COMMENT '用户表外键',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of doctors
-- ----------------------------
INSERT INTO `doctors` VALUES (2, 'doctor1@example.com', '13700000000', '2024-03-26 16:42:20', '2024-03-29 16:59:17', '吴喜', 2, '1996-03-27', 2);
INSERT INTO `doctors` VALUES (3, 'root@163.com', '13521214509', '2024-03-29 17:14:47', '2024-03-29 18:40:59', 'root', 1, '2024-02-29', 1013);

-- ----------------------------
-- Table structure for inventory_inbound
-- ----------------------------
DROP TABLE IF EXISTS `inventory_inbound`;
CREATE TABLE `inventory_inbound`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长的入库单ID',
  `drugId` int(11) NOT NULL COMMENT '药品ID，关联到药品表',
  `inboundQuantity` int(11) NOT NULL COMMENT '出入库数量',
  `inboundPrice` decimal(10, 2) NOT NULL COMMENT '出入库单价',
  `totalAmount` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '总金额（入库数量乘以入库单价）',
  `inboundReason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '出入库原因',
  `operatorId` int(11) NULL DEFAULT NULL COMMENT '操作员ID，关联到管理员表',
  `createdAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '出入库日期',
  `updatedAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `ioType` int(1) NOT NULL COMMENT '判断入库（1）、出库（2）、待出库（3）',
  `isDelete` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventory_inbound
-- ----------------------------
INSERT INTO `inventory_inbound` VALUES (1, 1, 100, 10.00, 1000.00, '新购入药品', 1, '2024-03-26 16:44:50', '2024-03-28 19:59:18', 1, 0);
INSERT INTO `inventory_inbound` VALUES (2, 2, 50, 8.00, 400.00, '补充库存', 2, '2024-03-26 16:44:50', '2024-03-28 19:59:18', 1, 0);
INSERT INTO `inventory_inbound` VALUES (3, 1, 10, 200.00, 2000.00, '按处方出库', 2, '2024-03-27 08:22:31', '2024-03-28 19:59:20', 2, 0);

-- ----------------------------
-- Table structure for medicine
-- ----------------------------
DROP TABLE IF EXISTS `medicine`;
CREATE TABLE `medicine`  (
  `medicineId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `medicineName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '药品名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `dosageForm` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '剂型',
  `specification` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格',
  `approvalHolder` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上市许可持有人',
  `manufacturer` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生产单位',
  `approvalNumber` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '批准文号',
  `usage` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用途',
  `medicineCode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '药品编码',
  `expirationDate` date NULL DEFAULT NULL COMMENT '保质期',
  `stockQuantity` int(11) NOT NULL DEFAULT 0 COMMENT '库存数量',
  `isDelete` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`medicineId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of medicine
-- ----------------------------
INSERT INTO `medicine` VALUES (1, '阿莫西林胶囊', 15.00, '胶囊剂', '0.25g*12粒', '某制药公司', '某工厂', '国药准字H20000001', '抗菌消炎', 'AMXLCJ01', '2023-12-31', 500, 0);
INSERT INTO `medicine` VALUES (2, '布洛芬缓释胶囊', 10.50, '胶囊剂', '0.3g*12粒', '某制药公司', '某工厂', '国药准字H20000002', '解热镇痛', 'BLFXHC01', '2024-12-31', 300, 0);

-- ----------------------------
-- Table structure for patient
-- ----------------------------
DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '患者的唯一标识符',
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '患者姓名',
  `gender` int(1) NOT NULL COMMENT '性别',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `birthdate` date NOT NULL COMMENT '出生日期',
  `createdAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '患者注册时间戳',
  `updatedAt` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '患者信息最后修改时间戳',
  `userId` int(11) NULL DEFAULT NULL COMMENT '用户表外键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of patient
-- ----------------------------
INSERT INTO `patient` VALUES (1, '张三', 1, '13800138000', '1990-01-01', '2024-03-26 16:42:20', '2024-03-27 17:22:06', 1003);
INSERT INTO `patient` VALUES (2, '李四', 2, '13900139000', '1995-05-05', '2024-03-26 16:42:20', '2024-03-27 17:22:09', 1004);
INSERT INTO `patient` VALUES (3, '王五', 1, '13700137000', '1988-11-11', '2024-03-26 16:42:20', '2024-03-27 17:22:11', 1005);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名，不能为空且要求唯一',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '123456' COMMENT '密码（已加密存储），不能为空',
  `role_type` int(1) NOT NULL COMMENT '用户类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1014 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 1);
INSERT INTO `user` VALUES (2, 'doctor1', '123456', 2);
INSERT INTO `user` VALUES (1003, 'zhangsan', '123456', 3);
INSERT INTO `user` VALUES (1004, 'lisi', '123456', 3);
INSERT INTO `user` VALUES (1005, 'wangwu', '123456', 3);
INSERT INTO `user` VALUES (1007, 'lx1008611', '123456', 3);
INSERT INTO `user` VALUES (1013, 'root01', '123456', 2);

SET FOREIGN_KEY_CHECKS = 1;
