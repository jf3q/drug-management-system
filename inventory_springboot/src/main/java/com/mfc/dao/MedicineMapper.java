package com.mfc.dao;

import com.mfc.entity.Medicine;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MedicineMapper {

    @Select("<script> SELECT * FROM medicine  where isDelete=0 "
            +"<if test=\"medicineCode != null and medicineCode !='' \"> and  medicineCode  like concat('%',#{medicineCode},'%')  </if>"
            +"<if test=\"medicineName != null and medicineName !='' \"> and  medicineName  like concat('%',#{medicineName},'%')  </if>"
            +"<if test=\"medicineId != null and medicineId !='' \"> and  medicineId=#{medicineId}  </if>"
            +"</script>")
    List<Medicine> list(Medicine o);

    @Update("<script>update medicine "
            +"<trim prefix=\"set\" suffixOverrides=\",\">"
            +"<if test=\"medicineName != null and medicineName != ''\">medicineName = #{medicineName},</if>"
            +"<if test=\"price != null and price != ''\">price = #{price},</if>"
            +"<if test=\"dosageForm != null and dosageForm != ''\">dosageForm = #{dosageForm},</if>"
            +"<if test=\"specification != null and specification != ''\">specification = #{specification},</if>"
            +"<if test=\"approvalHolder != null and approvalHolder != ''\">approvalHolder = #{approvalHolder},</if>"
            +"<if test=\"manufacturer != null and manufacturer != ''\">manufacturer = #{manufacturer},</if>"
            +"<if test=\"approvalNumber != null and approvalNumber != ''\">approvalNumber = #{approvalNumber},</if>"
            +"<if test=\"usage != null and usage != ''\">`usage` = #{usage},</if>"
            +"<if test=\"medicineCode != null and medicineCode != ''\">`medicineCode` = #{medicineCode},</if>"
            +"<if test=\"expirationDate != null \">`expirationDate` = #{expirationDate},</if>"
            +"<if test=\"stockQuantity != null and stockQuantity != 0\">`stockQuantity` = #{stockQuantity},</if>"
            +"</trim> where medicineId=#{medicineId}</script>")
    void update(Medicine o);

    @Update("update medicine set stockQuantity=stockQuantity+#{stockQuantity} where medicineId=#{medicineId}")
    void updateStockQuantity(Medicine o);

    @Insert("INSERT INTO medicine (medicineId, medicineName, price, dosageForm, specification, approvalHolder, manufacturer, approvalNumber, `usage`, medicineCode, expirationDate, stockQuantity) VALUES\n" +
            " (0, #{medicineName}, #{price}, #{dosageForm}, #{specification}, #{approvalHolder}, #{manufacturer}, #{approvalNumber},#{usage}, #{medicineCode}, #{expirationDate}, 0)")
    @Options(useGeneratedKeys=true, keyProperty="medicineId", keyColumn="medicineId")
    void insert(Medicine o);

    @Update("update medicine set isDelete=1 where  medicineId=#{medicineId}")
    void delete(Integer medicineId);

}
