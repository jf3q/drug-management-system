package com.mfc.dao;

import com.mfc.entity.InIngredients;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface  InIngredientsMapper {


	@Select("select * from in_ingredients where  id = #{0}")
    public InIngredients getById(Integer  id);

	@Insert("insert into in_ingredients (iname,cts,type_id,inventory,unit,idesc,code) values (#{iname},#{cts},#{type_id},#{inventory},#{unit},#{idesc},#{code}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InIngredients o);


	@Update("<script>update in_ingredients "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"iname != null and iname != ''\">iname = #{iname},</if>"
			+"<if test=\"type_id != null and type_id != ''\">type_id = #{type_id},</if>"
			+"<if test=\"unit != null and unit != ''\">unit = #{unit},</if>"
			+"<if test=\"inventory != null and inventory != ''\">inventory = #{inventory},</if>"
			+"<if test=\"idesc != null and idesc != ''\">idesc = #{idesc},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InIngredients o);


	@Select("<script>select i.*,t.tname from in_ingredients i left join in_type t on t.id=i.type_id where 1=1"
			+"<if test=\"iname != null and iname !='' \"> and  iname  like concat('%',#{iname},'%')  </if>"
			+"<if test=\"type_id != null and type_id !='' \"> and  type_id = #{type_id} </if>"
			+"<if test=\"code != null and code !='' \"> and  code = #{code} </if>"
			+"</script>")
    public List<InIngredients> list(InIngredients o);


	@Delete("delete from in_ingredients where id = #{0}")
	public void delete(Integer id);

}
