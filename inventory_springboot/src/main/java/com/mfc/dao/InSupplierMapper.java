package com.mfc.dao;

import com.mfc.entity.InSupplier;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface  InSupplierMapper {


	@Select("select * from in_supplier where  id = #{0}")
    public InSupplier getById(Integer  id);

	@Insert("insert into in_supplier (sname,cts,headname,headtel,headsex,sdesc) values (#{sname},#{cts},#{headname},#{headtel},#{headsex},#{sdesc}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InSupplier o);


	@Update("<script>update in_supplier "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"sname != null and sname != ''\">sname = #{sname},</if>"
			+"<if test=\"headname != null and headname != ''\">headname = #{headname},</if>"
			+"<if test=\"headsex != null and headsex != ''\">headsex = #{headsex},</if>"
			+"<if test=\"headtel != null and headtel != ''\">headtel = #{headtel},</if>"
			+"<if test=\"sdesc != null and sdesc != ''\">sdesc = #{sdesc},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InSupplier o);


	@Select("<script>select * from in_supplier where 1=1"
			+"<if test=\"sname != null and sname !='' \"> and  sname  like concat('%',#{sname},'%')  </if>"
			+"<if test=\"headname != null and headname !='' \"> and  headname like concat('%',#{headname},'%')  </if>"
			+"</script>")
    public List<InSupplier> list(InSupplier o);


	@Delete("delete from in_supplier where id = #{0}")
	public void delete(Integer id);

}
