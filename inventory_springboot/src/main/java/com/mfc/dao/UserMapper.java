package com.mfc.dao;

import com.mfc.dto.LoginDto;
import com.mfc.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("<script>select id,account,role_type as roleType from user  where 1=1"
            +"<if test=\"uname != null and uname !='' \"> and  account  = #{uname}  </if>"
            +"<if test=\"upass != null and upass !='' \"> and  password  = #{upass}  </if>"
            +"</script>")
    public List<User> list(LoginDto o);

    @Insert("insert into `user`(account,role_type) values(#{account},#{roleType}) ")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void insert(User u);

    @Delete("delete from `user` where id=#{userId}")
    void delete(Integer userId);

    @Select("<script>SELECT\n" +
            "u.id,u.account,u.role_type as roleType,IF(role_type=1,a.userName,d.userName) userName,\n" +
            "IF(role_type=1,a.gender,d.gender) gender,\n" +
            "IF(role_type=1,a.birthdate,d.birthdate) birthdate,\n" +
            "IF(role_type=1,a.email,d.email) email,\n" +
            "IF(role_type=1,a.phone,d.phone) phone,\n" +
            "IF(role_type=1,a.createdAt,d.createdAt) createdAt\n" +
            "FROM\n" +
            "\t`user` u\n" +
            "\tLEFT JOIN admins a ON a.userId = u.id\n" +
            "\tLEFT JOIN doctors d ON d.userId = u.id\n" +
            "\twhere u.role_type in (1,2)"
            +"<if test=\"userName != null and userName !='' \"> \tand (a.userName LIKE CONCAT('%',#{userName},'%') or d.userName LIKE CONCAT('%',#{userName},'%'))  </if>"
            +"<if test=\"gender != null and gender !=0   \"> and (a.gender=#{gender} or d.gender=#{gender})  </if>"
            +"<if test=\"id != null and id !=0 \"> and  u.id  = #{id}  </if>"
            +"<if test=\"roleType != null and roleType !=0 \"> and u.role_type=#{roleType}  </if>"
            +"</script>")
    List<User> getList(User o);

    @Update("update `user` set password=#{password} where id=#{id}")
    void updatePass(User userDto);
    @Update("update `user` set account=#{account} where id=#{id}")
    void updateAccount(User o);

    @Select("select * from `user` where id=#{id}")
    User getById(Integer id);

    @Select("select * from `user` where account=#{account}")
    List<User> getByUserAccountList(String  account);
}
