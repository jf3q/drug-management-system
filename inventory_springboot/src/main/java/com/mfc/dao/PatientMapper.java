package com.mfc.dao;

import com.mfc.entity.Patient;
import com.mfc.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PatientMapper {

    @Select("<script>select * from patient where 1=1"
            +"<if test=\"userName != null and userName !='' \"> and  userName  like concat('%',#{userName},'%')   </if>"
            +"<if test=\"gender != null and gender > 0 \"> and  gender  = #{gender}  </if>"
            +"</script>")
    List<Patient> list(User o);

    @Select("select p.*,u.account from patient p join `user` u on u.id=p.userId  where p.id=#{id}")
    Patient getById(Integer id);

    @Insert("INSERT into patient(userName,gender,phone,birthdate,userId) VALUES(#{userName},#{gender},#{phone},#{birthdate},#{userId})")
    void insert(Patient o);

    @Delete("delete from patient where userId=#{id}")
    void delete(Integer id);

    @Update("<script>update patient "
            +"<trim prefix=\"set\" suffixOverrides=\",\">"
            +"<if test=\"userName != null and userName != ''\">userName = #{userName},</if>"
            +"<if test=\"gender != null and gender != 0\">gender = #{gender},</if>"
            +"<if test=\"phone != null and phone != ''\">phone = #{phone},</if>"
            +"<if test=\"birthdate != null \">birthdate = #{birthdate},</if>"
            +"</trim> where userId=#{id}</script>")
    void update(Patient o);
}
