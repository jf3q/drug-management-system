package com.mfc.dao;

import com.mfc.entity.InType;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface   InTypeMapper {


	@Select("select * from in_type where  id = #{0}")
    public InType getById(Integer  id);

	@Insert("insert into in_type (tname,cts) values (#{tname},#{cts}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InType o);


	@Update("<script>update in_type "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"tname != null and tname != ''\">tname = #{tname},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InType o);


	@Select("<script>select * from in_type where 1=1"
			+"<if test=\"tname != null and tname !='' \"> and  tname  like concat('%',#{tname},'%')  </if>"
			+"</script>")
    public List<InType> list(InType o);


	@Delete("delete from in_type where id = #{0}")
	public void delete(Integer id);

}
