package com.mfc.dao;

import com.mfc.entity.InReceipt;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface  InReceiptMapper {


	@Select("select r.*,i.iname,i.code,t.tname,a.realname from in_receipt r left join in_ingredients i on i.id=r.ingredients_id left join in_type t on t.id=i.type_id  left join in_staff a on a.id=r.buyer_id where r.id = #{0}")
    public InReceipt getById(Integer  id);

	@Insert("insert into in_receipt ( cts,ingredients_id,buyer_id,total_money,buy_num,unit,price,wreck,wreck_desc,storage_note,status,buy_ts,storage_ts,supplier_msg,buy_note,buyed_ts,buyed_note,buyed_num,storage_num) values (#{cts},#{ingredients_id},#{buyer_id},#{total_money},#{buy_num},#{unit},#{price},#{wreck},#{wreck_desc},#{storage_note},#{status},#{buy_ts},#{storage_ts},#{supplier_msg},#{buy_note},#{buyed_ts},#{buyed_note},#{buyed_num},#{storage_num}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InReceipt o);


	@Update("<script>update in_receipt "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"ingredients_id != null and ingredients_id != ''\">ingredients_id = #{ingredients_id},</if>"
			+"<if test=\"buyer_id != null and buyer_id != ''\">buyer_id = #{buyer_id},</if>"
			+"<if test=\"total_money != null and total_money != ''\">total_money = #{total_money},</if>"
			+"<if test=\"buy_num != null and buy_num != ''\">buy_num = #{buy_num},</if>"
			+"<if test=\"unit != null and unit != ''\">unit = #{unit},</if>"
			+"<if test=\"price != null and price != ''\">price = #{price},</if>"
			+"<if test=\"wreck != null and wreck != ''\">wreck = #{wreck},</if>"
			+"<if test=\"wreck_desc != null and wreck_desc != ''\">wreck_desc = #{wreck_desc},</if>"
			+"<if test=\"storage_note != null and storage_note != ''\">storage_note = #{storage_note},</if>"
			+"<if test=\"status != null and status != ''\">status = #{status},</if>"
			+"<if test=\"buy_ts != null and buy_ts != ''\">buy_ts = #{buy_ts},</if>"
			+"<if test=\"storage_ts != null and storage_ts != ''\">storage_ts = #{storage_ts},</if>"
			+"<if test=\"supplier_msg != null and supplier_msg != ''\">supplier_msg = #{supplier_msg},</if>"
			+"<if test=\"buy_note != null and buy_note != ''\">buy_note = #{buy_note},</if>"
			+"<if test=\"buyed_ts != null and buyed_ts != ''\">buyed_ts = #{buyed_ts},</if>"
			+"<if test=\"buyed_note != null and buyed_note != ''\">buyed_note = #{buyed_note},</if>"
			+"<if test=\"buyed_num != null and buyed_num != ''\">buyed_num = #{buyed_num},</if>"
			+"<if test=\"storage_num != null and storage_num != ''\">storage_num = #{storage_num},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InReceipt o);


	@Select("<script>select r.*,i.iname,i.code,t.tname,a.realname from in_receipt r left join in_ingredients i on i.id=r.ingredients_id left join in_type t on t.id=i.type_id left join in_staff a on a.id=r.buyer_id where 1=1"
			+"<if test=\"iname != null and iname !='' \"> and  i.iname  like concat('%',#{iname},'%')  </if>"
			+"<if test=\"realname != null and realname !='' \"> and  a.realname  like concat('%',#{realname},'%')  </if>"
			+"<if test=\"type_id != null and type_id !='' \"> and  i.type_id  = #{type_id}  </if>"
			+"<if test=\"status != null and status !='' \"> and  r.status  = #{status}  </if>"
			+"<if test=\"buyer_id != null and buyer_id !='' \"> and  r.buyer_id  = #{buyer_id}  </if>"
			+"<if test=\"code != null and code !='' \"> and  i.code  = #{code}  </if>"
			+"</script>")
    public List<InReceipt> list(InReceipt o);


	@Delete("delete from in_receipt where id = #{0}")
	public void delete(Integer id);

}
