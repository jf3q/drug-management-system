package com.mfc.dao;

import com.mfc.entity.InWreck;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface   InWreckMapper {


	@Select("select * from in_wreck where  id = #{0}")
    public InWreck getById(Integer  id);

	@Insert("insert into in_wreck (wreck,wreck_desc,ingredients_id,unit,cts) values (#{wreck},#{wreck_desc},#{ingredients_id},#{unit},#{cts}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InWreck o);


	@Update("<script>update in_wreck "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"wreck != null and wreck != ''\">wreck = #{wreck},</if>"
			+"<if test=\"wreck_desc != null and wreck_desc != ''\">wreck_desc = #{wreck_desc},</if>"
			+"<if test=\"ingredients_id != null and ingredients_id != ''\">ingredients_id = #{ingredients_id},</if>"
			+"<if test=\"unit != null and unit != ''\">unit = #{unit},</if>"
			+"<if test=\"cts != null and cts != ''\">cts = #{cts},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InWreck o);


	@Select("<script>select w.*,t.tname,i.iname,i.code,i.type_id from in_wreck w left join in_ingredients i on w.ingredients_id=i.id left join in_type t  on t.id=i.type_id where 1=1"
			+"<if test=\"iname != null and iname !='' \"> and  i.iname  like concat('%',#{iname},'%')  </if>"
			+"<if test=\"type_id != null and type_id !='' \"> and i.type_id = #{type_id} </if>"
			+"<if test=\"code != null and code !='' \"> and  i.code = #{code} </if>"
			+"</script>")
    public List<InWreck> list(InWreck o);


	@Delete("delete from in_wreck where id = #{0}")
	public void delete(Integer id);

}
