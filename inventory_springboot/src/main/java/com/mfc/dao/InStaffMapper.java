package com.mfc.dao;

import com.mfc.entity.InStaff;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface  InStaffMapper {


	@Select("select * from in_staff where  id = #{0}")
    public InStaff getById(Integer  id);

	@Insert("insert into in_staff (uname,upass,cts,realname,sex,borth,role,status,msg,status_ts) values (#{uname},#{upass},#{cts},#{realname},#{sex},#{borth},#{role},#{status},#{msg},#{status_ts}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InStaff o);


	@Update("<script>update in_staff "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"uname != null and uname != ''\">uname = #{uname},</if>"
			+"<if test=\"upass != null and upass != ''\">upass = #{upass},</if>"
			+"<if test=\"realname != null and realname != ''\">realname = #{realname},</if>"
			+"<if test=\"sex != null and sex != ''\">sex = #{sex},</if>"
			+"<if test=\"borth != null and borth != ''\">borth = #{borth},</if>"
			+"<if test=\"role != null and role != ''\">role = #{role},</if>"
			+"<if test=\"status != null and status != ''\">status = #{status},</if>"
			+"<if test=\"msg != null and msg != ''\">msg = #{msg},</if>"
			+"<if test=\"status_ts != null and status_ts != ''\">status_ts = #{status_ts},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InStaff o);


	@Select("<script>select * from in_staff where 1=1"
			+"<if test=\"uname != null and uname !='' \"> and  uname  = #{uname}  </if>"
			+"<if test=\"status != null and status !='' \"> and  status  = #{status}  </if>"
			+"<if test=\"role != null and role !='' \"> and  role  = #{role}  </if>"
			+"<if test=\"realname != null and realname !='' \"> and  realname  like concat('%',#{realname},'%') </if>"
			+"</script>")
    public List<InStaff> list(InStaff o);


	@Delete("delete from in_staff where id = #{0}")
	public void delete(Integer id);

}
