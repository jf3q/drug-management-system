package com.mfc.dao;

import com.mfc.entity.Admin;
import com.mfc.entity.User;
import org.apache.ibatis.annotations.*;

@Mapper
public interface AdminMapper {

    @Select("select a.*,u.account from admins a join `user` u on u.id=a.userId  where a.userId=#{id}")
    Admin selectByUserId(Integer id);

    @Delete("delete from admins where userId=#{id}")
    void delete(Integer id);

    @Insert("insert into admins( userName, email, phone,userId,gender,birthdate) VALUES\n" +
            "(#{userName}, #{email}, #{phone}, #{id},#{gender},#{birthdate})")
    void insert(User o);

    @Update("<script>update admins "
            +"<trim prefix=\"set\" suffixOverrides=\",\">"
            +"<if test=\"userName != null and userName != ''\">userName = #{userName},</if>"
            +"<if test=\"gender != null and gender != 0\">gender = #{gender},</if>"
            +"<if test=\"phone != null and phone != ''\">phone = #{phone},</if>"
            +"<if test=\"birthdate != null \">birthdate = #{birthdate},</if>"
            +"</trim> where userId=#{id}</script>")
    void update(User o);
}
