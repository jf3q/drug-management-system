package com.mfc.dao;

import com.mfc.entity.Doctor;
import com.mfc.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DoctorMapper {

    @Select("select d.*,u.account from doctors d join `user` u on u.id=d.userId  where d.userId=#{id}")
    Doctor selectByUserId(Integer id);

    @Select("<script>select d.*,u.account from doctors d left join user u on u.id=d.userId  where 1=1"
            +"<if test=\"userName != null and userName !='' \"> and  userName  like concat('%',#{userName},'%')   </if>"
            +"<if test=\"gender != null and gender > 0 \"> and  gender  = #{gender}  </if>"
            +"</script>")
    List<Doctor> list(User o);

    @Delete("delete from doctors where userId=#{id}")
    void delete(Integer id);

    @Insert("insert into doctors(userName, email, phone,userId,gender,birthdate) VALUES\n" +
            "(#{userName}, #{email}, #{phone}, #{id},#{gender},#{birthdate})")
    void insert(User o);

    @Update("<script>update doctors "
            +"<trim prefix=\"set\" suffixOverrides=\",\">"
            +"<if test=\"userName != null and userName != ''\">userName = #{userName},</if>"
            +"<if test=\"gender != null and gender != 0\">gender = #{gender},</if>"
            +"<if test=\"phone != null and phone != ''\">phone = #{phone},</if>"
            +"<if test=\"birthdate != null \">birthdate = #{birthdate},</if>"
            +"</trim> where userId=#{id}</script>")
    void update(User o);
}
