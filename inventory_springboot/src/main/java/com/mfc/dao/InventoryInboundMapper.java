package com.mfc.dao;

import com.mfc.entity.InventoryInbound;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InventoryInboundMapper {

    @Select("<script> SELECT\n" +
            "\t ii.*,m.medicineName,m.manufacturer,IF(u.role_type=1,a.userName,d.userName) as userName\n" +
            " FROM\n" +
            "\t inventory_inbound ii \n" +
            "\t LEFT JOIN medicine m on  m.medicineId = ii.drugId\n" +
            "\t LEFT JOIN `user` u on u.id=ii.operatorId\n" +
            "\t LEFT JOIN admins a on a.userId=u.id\n" +
            "\t LEFT JOIN doctors d on d.userId=u.id where ii.isDelete=0  "
            +"<if test=\"medicineName != null and medicineName !='' \"> and  m.medicineName  like concat('%',#{medicineName},'%')  </if>"
            +"<if test=\"operatorId != null and operatorId !='' \"> and  ii.operatorId = #{operatorId} </if>"
            +"<if test=\"createdAt != null \"> and  ii.createdAt = #{createdAt} </if>"
            +"<if test=\"userName != null and userName !='' \">   and (a.userName LIKE CONCAT('%',#{userName},'%') or d.userName LIKE CONCAT('%',#{userName},'%'))  </if>"
            +"<if test=\"manufacturer != null and manufacturer !='' \"> and  m.manufacturer= #{manufacturer} </if>"
            +"<if test=\"id != null and id !=0 \"> and  ii.id= #{id} </if>"
            +"<if test=\"ioType != null and ioType !=0 \"> and  ii.ioType= #{ioType} </if>"
            +"<if test=\"ioType == 0  \"> and  ii.ioType in(2,3) </if>"
            +"</script>")
    List<InventoryInbound> list(InventoryInbound o);

    @Update("update inventory_inbound set isDelete=1 where id=#{id}")
    void delete(Integer id);

    @Insert("insert into inventory_inbound (drugId, inboundQuantity, inboundPrice, totalAmount, inboundReason, operatorId,ioType) VALUES" +
            "(#{drugId},#{inboundQuantity},#{inboundPrice},#{totalAmount},#{inboundReason},#{operatorId},#{ioType})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void insert(InventoryInbound o);

    @Update("update inventory_inbound set ioType=2 where id=#{id} ")
    void updateIoType(InventoryInbound o);
}
