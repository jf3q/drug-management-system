package com.mfc.dao;

import com.mfc.entity.InDetail;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface   InDetailMapper {


	@Select("select * from in_detail where  id = #{0}")
    public InDetail getById(Integer  id);

	@Insert("insert into in_detail (inventory,ddesc,ingredients_id,unit,cts,type,from_id) values (#{inventory},#{ddesc},#{ingredients_id},#{unit},#{cts},#{type},#{from_id}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	public void insert(InDetail o);


	@Update("<script>update in_detail "
			+"<trim prefix=\"set\" suffixOverrides=\",\">"
			+"<if test=\"inventory != null and inventory != ''\">inventory = #{inventory},</if>"
			+"<if test=\"ddesc != null and ddesc != ''\">ddesc = #{ddesc},</if>"
			+"<if test=\"ingredients_id != null and ingredients_id != ''\">ingredients_id = #{ingredients_id},</if>"
			+"<if test=\"unit != null and unit != ''\">unit = #{unit},</if>"
			+"<if test=\"cts != null and cts != ''\">cts = #{cts},</if>"
			+"</trim> where id=#{id}</script>")
    public void update(InDetail o);


	@Select("<script>select d.*,t.tname,i.iname,i.`code`,i.type_id from in_detail d left join in_ingredients i on d.ingredients_id=i.id left join in_type t  on t.id=i.type_id where 1=1"
			+"<if test=\"iname != null and iname !='' \"> and  i.iname  like concat('%',#{iname},'%')  </if>"
			+"<if test=\"type_id != null and type_id !='' \"> and  i.type_id = #{type_id} </if>"
			+"<if test=\"code != null and code !='' \"> and  i.code = #{code} </if>"
			+"<if test=\"from_id != null and from_id !='' \"> and  d.from_id = #{from_id} </if>"
			+"</script>")
    public List<InDetail> list(InDetail o);


	@Delete("delete from in_detail where id = #{0}")
	public void delete(Integer id);

}
