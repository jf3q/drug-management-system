package com.mfc.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginDto {

    private String uname; //账号
    private String upass; //密码
}
