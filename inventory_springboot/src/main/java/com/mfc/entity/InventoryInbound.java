package com.mfc.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class InventoryInbound {;
    //自增长的入库单ID
    private Integer id;
    //药品ID，关联到药品表
    private Integer drugId;
    //出入库数量
    private Integer inboundQuantity;
    //出入库单价
    private Double inboundPrice;
    //总金额（入库数量乘以入库单价）
    private Double totalAmount;
    //出入库原因
    private String inboundReason;
    //操作员ID，关联到管理员表
    private Integer operatorId;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    //更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
    //判断入库（1）、出库（2）、待出库（3）
    private Integer ioType;


    //药品名称
    private String medicineName;
    //操作员名称
    private String userName;
    //生产单位
    private String manufacturer;
    //是否删除
    private Integer isDelete;
}
