package com.mfc.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Accessors(chain = true)
public class Medicine {

    private Integer  medicineId;            //主键
    private String  medicineName;           //药品名称
    private BigDecimal price;               //价格
    private String  dosageForm;             //剂型
    private String  specification;          //规格
    private String  approvalHolder;         //上市许可持有人
    private String  manufacturer;           //生产单位
    private String  approvalNumber;         //批准文号
    private String  usage;                  //用途
    private String  medicineCode;           //药品编码
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expirationDate;            //保质期
    private Integer  stockQuantity;         //库存数量

}
