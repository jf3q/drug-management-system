package com.mfc.entity;

public class   InDetail{
	private Integer id;  
	private Double inventory;
	private String ddesc;
	private Integer ingredients_id;
	private String unit;
	private String cts;
	private String type;
	private String from_id;
	/*----*/
	private String iname;
	private String code;
	private String tname;
	private Integer type_id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIname() {
		return iname;
	}
	public void setIname(String iname) {
		this.iname = iname;
	}
	public String getCts() {
		return cts;
	}
	public Double getInventory() {
		return inventory;
	}
	
	public String getFrom_id() {
		return from_id;
	}
	public void setFrom_id(String from_id) {
		this.from_id = from_id;
	}
	public void setInventory(Double inventory) {
		this.inventory = inventory;
	}
	public String getDdesc() {
		return ddesc;
	}
	public void setDdesc(String ddesc) {
		this.ddesc = ddesc;
	}
	public Integer getIngredients_id() {
		return ingredients_id;
	}
	public void setIngredients_id(Integer ingredients_id) {
		this.ingredients_id = ingredients_id;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public Integer getType_id() {
		return type_id;
	}
	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
	public void setCts(String cts) {
		this.cts = cts;
	}
	 
	 
	 
	 
	 

	
}
