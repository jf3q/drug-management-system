package com.mfc.entity;

public class InStaff {
	private Integer id;
	private String uname; 
	private String upass;
	private String cts;
	private String realname;
	private String sex;
	private String borth;
	private String role;
	private String status;//0禁用1启用
	private String status_ts;//0禁用1启用
	private String msg;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUpass() {
		return upass;
	}
	public void setUpass(String upass) {
		this.upass = upass;
	}
	public String getCts() {
		return cts;
	}
	public void setCts(String cts) {
		this.cts = cts;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBorth() {
		return borth;
	}
	public void setBorth(String borth) {
		this.borth = borth;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getStatus_ts() {
		return status_ts;
	}
	public void setStatus_ts(String status_ts) {
		this.status_ts = status_ts;
	}
	 
	 
	 
	 
	 

	
}
