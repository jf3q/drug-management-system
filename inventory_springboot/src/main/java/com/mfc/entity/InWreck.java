package com.mfc.entity;

public class   InWreck {
	private Integer id;  
	private Double wreck;
	private String wreck_desc;
	private Integer ingredients_id;
	private String unit;
	private String cts;
	 
	/*----*/
	private String iname;
	private String code;
	private String tname;
	private Integer type_id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIname() {
		return iname;
	}
	public void setIname(String iname) {
		this.iname = iname;
	}
	public String getCts() {
		return cts;
	}
	public Double getWreck() {
		return wreck;
	}
	public void setWreck(Double wreck) {
		this.wreck = wreck;
	}
	public String getWreck_desc() {
		return wreck_desc;
	}
	public void setWreck_desc(String wreck_desc) {
		this.wreck_desc = wreck_desc;
	}
	public Integer getIngredients_id() {
		return ingredients_id;
	}
	public void setIngredients_id(Integer ingredients_id) {
		this.ingredients_id = ingredients_id;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setCts(String cts) {
		this.cts = cts;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public Integer getType_id() {
		return type_id;
	}
	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
	 
	 
	 
	 
	 

	
}
