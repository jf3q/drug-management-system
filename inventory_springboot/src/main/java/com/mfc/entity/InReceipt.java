package com.mfc.entity;

public class  InReceipt {
	private Integer id;  
	private String cts;
	private Integer ingredients_id;  
	private Integer buyer_id;  
	private Double total_money;  
	private Double buy_num;  
	private String unit;  
	private Double price;  
	private Double wreck; 
	private Double storage_num; 
	private String wreck_desc;  
	private String storage_note;  
	private String status; //0初始，代采购，1已采购，2已入库 
	private String buy_ts;  
	private String storage_ts;  
	private String supplier_msg;  
	private String buy_note;
	private String buyed_ts;
	private String buyed_note;
	
	
	
	/*--*/ 
	private String iname;
	private String tname;
	private String realname;
	private String code;
	private Integer type_id;
	private Double buyed_num; 
	
	
	public Double getStorage_num() {
		return storage_num;
	}
	public void setStorage_num(Double storage_num) {
		this.storage_num = storage_num;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCts() {
		return cts;
	}
	public void setCts(String cts) {
		this.cts = cts;
	}
	public Integer getIngredients_id() {
		return ingredients_id;
	}
	public void setIngredients_id(Integer ingredients_id) {
		this.ingredients_id = ingredients_id;
	}
	public Integer getBuyer_id() {
		return buyer_id;
	}
	public void setBuyer_id(Integer buyer_id) {
		this.buyer_id = buyer_id;
	}
	public Double getTotal_money() {
		return total_money;
	}
	public void setTotal_money(Double total_money) {
		this.total_money = total_money;
	}
	public Double getBuy_num() {
		return buy_num;
	}
	public void setBuy_num(Double buy_num) {
		this.buy_num = buy_num;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getWreck() {
		return wreck;
	}
	public void setWreck(Double wreck) {
		this.wreck = wreck;
	}
	public String getWreck_desc() {
		return wreck_desc;
	}
	public void setWreck_desc(String wreck_desc) {
		this.wreck_desc = wreck_desc;
	}
	public String getStorage_note() {
		return storage_note;
	}
	public void setStorage_note(String storage_note) {
		this.storage_note = storage_note;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBuy_ts() {
		return buy_ts;
	}
	public void setBuy_ts(String buy_ts) {
		this.buy_ts = buy_ts;
	}
	public String getStorage_ts() {
		return storage_ts;
	}
	public void setStorage_ts(String storage_ts) {
		this.storage_ts = storage_ts;
	}
	public String getSupplier_msg() {
		return supplier_msg;
	}
	public void setSupplier_msg(String supplier_msg) {
		this.supplier_msg = supplier_msg;
	}
	public String getBuy_note() {
		return buy_note;
	}
	public void setBuy_note(String buy_note) {
		this.buy_note = buy_note;
	}
	public String getIname() {
		return iname;
	}
	public void setIname(String iname) {
		this.iname = iname;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public Integer getType_id() {
		return type_id;
	}
	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
	public String getBuyed_ts() {
		return buyed_ts;
	}
	public void setBuyed_ts(String buyed_ts) {
		this.buyed_ts = buyed_ts;
	}
	public String getBuyed_note() {
		return buyed_note;
	}
	public void setBuyed_note(String buyed_note) {
		this.buyed_note = buyed_note;
	}
	public Double getBuyed_num() {
		return buyed_num;
	}
	public void setBuyed_num(Double buyed_num) {
		this.buyed_num = buyed_num;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}  
	 
	 
	 
	 

	
}
