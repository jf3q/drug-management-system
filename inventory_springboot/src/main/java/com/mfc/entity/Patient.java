package com.mfc.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Patient {
    private Integer id;

    private String userName;

    private Integer gender;

    private String phone;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;

    private Date createdAt;

    private Date updatedAt;

    private Integer userId;

    private String account;
}
