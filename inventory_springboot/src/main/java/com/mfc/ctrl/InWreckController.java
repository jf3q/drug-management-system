package com.mfc.ctrl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mfc.cons.Sys;
import com.mfc.entity.InventoryInbound;
import com.mfc.entity.Medicine;
import com.mfc.service.InventoryInboundService;
import com.mfc.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/jsp/wreck")
public class  InWreckController {


	@Autowired
	private InventoryInboundService inboundService;

	@Autowired
	private MedicineService medicineService;

	@RequestMapping("/page")
	public String page(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			, InventoryInbound o, Model model) {
		PageHelper.startPage(pageNo,Sys.pageSize," id desc ");
		if (o.getIoType() == null){
			o.setIoType(0);
		}
		List<InventoryInbound> li = inboundService.list(o);

		PageInfo<InventoryInbound> pageInfo=new PageInfo<InventoryInbound>(li, Sys.pageSize);

		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("o", o);

		return Sys.jsp+"/wreck_page";
	}


	@RequestMapping("/toedit")
	public String toedit(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InventoryInbound o,Model model) {
		if(o.getId()!=null){
			o=inboundService.list(o).get(0);
			model.addAttribute("o", o);
		}
		model.addAttribute("pageNo", pageNo);
		return Sys.jsp+"/wreck_add";
	}

	/**
	 * 添加入库
	 */
	@RequestMapping("/save")
	public String save(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InventoryInbound o,Model model) {
		    o.setTotalAmount(o.getInboundPrice()*o.getInboundQuantity());
			o.setIoType(1);
			medicineService.updateStockQuantity(new Medicine().setStockQuantity(o.getInboundQuantity()).setMedicineId(o.getDrugId()));
			inboundService.insert(o);
		return "redirect:/"+Sys.jsp+"/detail/page?pageNo="+pageNo;
	}




	@RequestMapping("/del")
	public String del(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InventoryInbound o,Model model) {
		try {
			if (o.getIoType() == 3){
				medicineService.updateStockQuantity(new Medicine().setMedicineId(o.getDrugId()).setStockQuantity(o.getInboundQuantity()));
			}
			inboundService.delete(o.getId());
		} catch (Exception e) {
			model.addAttribute("msg", "请先删除该库存关联的其他信息");
			return "forward:/"+Sys.jsp+"/detail/page?pageNo="+pageNo;
		}
		return "redirect:/"+Sys.jsp+"/detail/page?pageNo="+pageNo;
	}

	@RequestMapping("/updateKu")
	public String updateKu(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InventoryInbound o,Model model){
		inboundService.updateIoType(o);
		return "redirect:/"+Sys.jsp+"/detail/page?pageNo="+pageNo;
	}



}
