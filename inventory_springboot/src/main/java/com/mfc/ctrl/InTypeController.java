package com.mfc.ctrl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mfc.cons.Sys;
import com.mfc.entity.InType;
import com.mfc.service.InTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/jsp/type")
public class  InTypeController {

	@Autowired
	private InTypeService typeService;




	@RequestMapping("/page")
	public String page(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InType o,Model model) {
		PageHelper.startPage(pageNo,Sys.pageSize," id desc ");
		List<InType> li=typeService.list(o);
		PageInfo<InType> pageInfo=new PageInfo<InType>(li, Sys.pageSize);

		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("o", o);

		return Sys.jsp+"/type_page";
	}


	@RequestMapping("/toedit")
	public String toedit(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InType o,Model model) {
		if(o.getId()!=null){
			o=typeService.getById(o.getId());
			model.addAttribute("o", o);
		}
		model.addAttribute("pageNo", pageNo);
		return Sys.jsp+"/type_add";
	}

	@RequestMapping("/save")
	public String save(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InType o,Model model) {
		if(o.getId()!=null){
			 typeService.update(o);
		}else{
			List<InType> li=typeService.list(o);
			if(li!=null&&li.size()>0){
				model.addAttribute("msg","该用户名已存在，请换一个");
				return Sys.jsp+"/type_add";
			}
			typeService.insert(o);
		}
		return "redirect:/"+Sys.jsp+"/type/page?pageNo="+pageNo;
	}




	@RequestMapping("/del")
	public String del(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InType o,Model model) {
		try {
			typeService.delete(o.getId());
		} catch (Exception e) {
			model.addAttribute("msg", "请先删除该分类关联的其他信息");
			return "forward:/"+Sys.jsp+"/type/page?pageNo="+pageNo;
		}
		return "redirect:/"+Sys.jsp+"/type/page?pageNo="+pageNo;
	}


}
