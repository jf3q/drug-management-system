package com.mfc.ctrl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mfc.cons.Sys;
import com.mfc.dto.LoginDto;
import com.mfc.entity.Admin;
import com.mfc.entity.Doctor;
import com.mfc.entity.Patient;
import com.mfc.entity.User;
import com.mfc.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/jsp/staff")
public class InStaffController {

	@Autowired
	private InStaffService staffService;

	@Autowired
	private UserService userService;

	@Autowired
	private DoctorService doctorService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private PatientService patientService;

	@RequestMapping("/info")
	public String info(User o,HttpServletRequest req,HttpSession session,Model model) {
		o=(User) session.getAttribute("staffU");
		if (o.getRoleType() == 1){
			Admin admin = adminService.selectByUserId(o.getId());
			BeanUtils.copyProperties(admin,o);
		}else if (o.getRoleType() == 2){
			Doctor doctor = doctorService.selectByUserId(o.getId());
			BeanUtils.copyProperties(doctor,o);
		}else if (o.getRoleType() == 3){
			Patient patient = patientService.selectByUserId(o.getId());
			BeanUtils.copyProperties(patient,o);
		}

		model.addAttribute("o", o);
		return Sys.jsp+"/staff_info";
	}

	@RequestMapping("/tologin")
	public String tologin(User o,HttpServletRequest req,HttpSession session,Model model) {
		  return Sys.jsp+"/login";
	}

	@RequestMapping("/logout")
	public String logout(User o,HttpServletRequest req,HttpSession session,Model model) {
		session.removeAttribute("staffU");
		return Sys.jsp+"/login";
	}

	@RequestMapping("/login")
	public String login(LoginDto o, HttpServletRequest req, HttpSession session, Model model) {

		if(o.getUname()==null||o.getUname().isEmpty()) {
			model.addAttribute("msg", "请输入用户名");
			return Sys.jsp+"/login";
		}

		if(o.getUpass()==null||o.getUpass().isEmpty()) {
			model.addAttribute("msg", "请输入密码");
			return Sys.jsp+"/login";
		}
		List<User> li = userService.list(o);
		if(li!=null&&li.size()>0){
			User user = li.get(0);
			User vo = new User();
			vo.setAccount(o.getUname());
			vo.setRoleType(user.getRoleType());
			if (user.getRoleType() == 1){
				Admin admin = adminService.selectByUserId(user.getId());
				BeanUtils.copyProperties(admin,vo);
				session.setAttribute("staffU", vo);
			}else if (user.getRoleType() == 2){
			    Doctor doctor = doctorService.selectByUserId(user.getId());
				BeanUtils.copyProperties(doctor,vo);
				session.setAttribute("staffU", vo);
			}else if (user.getRoleType() == 3){
				Patient patient = patientService.selectByUserId(user.getId());
				BeanUtils.copyProperties(patient,vo);
				session.setAttribute("staffU", vo);
			}
			 return Sys.jsp+"/index";
		}
		model.addAttribute("msg", "账号或密码错误");
		return Sys.jsp+"/login";
	}


	@RequestMapping("/toUpdateUpass")
	public String toUpdateUpass(User o,HttpServletRequest req,HttpSession session,Model model) {
		  return Sys.jsp+"/update_upass";
	}

	@RequestMapping("/updateUpass")
	public String updateUpass(User o,HttpServletRequest req,HttpSession session,Model model) {
		o=(User) session.getAttribute("staffU");
		String old_upass=req.getParameter("old_upass");
		String new_upass=req.getParameter("new_upass");
		String new_upass2=req.getParameter("new_upass2");

		o = userService.getById(o.getId());
		if(!o.getPassword().equals(old_upass)){
			model.addAttribute("msg", "原密码不对");
			return Sys.jsp+"/update_upass";
		}
		if(!new_upass.equals(new_upass2)){
			model.addAttribute("msg", "新密码不一致");
			return Sys.jsp+"/update_upass";
		}
		userService.updatePass(new User().setPassword(new_upass).setId(o.getId()));
		model.addAttribute("msg", "密码修改成功");
		return Sys.jsp+"/update_upass";
	}


	@RequestMapping("/page")
	public String page(@RequestParam(value="pageNo",defaultValue="1")Integer pageNo
			, User o, Model model) {
		PageHelper.startPage(pageNo,Sys.pageSize," u.id desc ");
		List<User>  li = userService.getList(o);
		PageInfo<User> pageInfo=new PageInfo<User>(li, Sys.pageSize);
		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("o", o);

		return Sys.jsp+"/staff_page";
	}


	@RequestMapping("/toedit")
	public String toedit(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,User o,Model model) {
		if(o.getId()!=null){
			User User = userService.getList(o).get(0);
			model.addAttribute("o", User);
		}
		model.addAttribute("pageNo", pageNo);
		return Sys.jsp+"/staff_add";
	}

	@RequestMapping("/save")
	public String save(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,User o,Model model) {
		if(o.getId()!=null){
			userService.updateAccount(o);
			if (o.getRoleType()==1){
				adminService.update(o);
			}else{
				doctorService.update(o);
			}
		}else{
			List<User> li = userService.getByUserAccountList(o.getAccount());
			if(li!=null&&li.size()>0){
				model.addAttribute("msg","该用户名已存在，请换一个");
				return Sys.jsp+"/staff_add";
			}
			userService.insert(o);
			if (o.getRoleType()==1){
				adminService.insert(o);
			}else{
				doctorService.insert(o);
			}
		}



		return "redirect:/"+Sys.jsp+"/staff/page?pageNo="+pageNo;
	}

	@RequestMapping("/initUpass")
	public String initUpass(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,User o,Model model) {
		o.setPassword(Sys.default_pwd);
		userService.updatePass(o);
		model.addAttribute("msg", "初始化密码："+Sys.default_pwd);
		return "forward:/"+Sys.jsp+"/staff/page?pageNo="+pageNo;
	}



	@RequestMapping("/del")
	public String del(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,User o,Model model) {
		try {
			if (o.getRoleType()==1){
				adminService.delete(o.getId());
			}else if (o.getRoleType()==2){
				doctorService.delete(o.getId());
			}
			userService.delete(o.getId());
		} catch (Exception e) {
			model.addAttribute("msg", "请先删除该用户关联的其他信息");
			return "forward:/"+Sys.jsp+"/staff/page?pageNo="+pageNo;
		}
		return "redirect:/"+Sys.jsp+"/staff/page?pageNo="+pageNo;
	}


	@ResponseBody
	@RequestMapping("/checkUname")
	public Object checkUname(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,LoginDto o,Model model) {

		Map<String,Object> m=new HashMap<String,Object>();
		List<User> li = userService.list(o);
		if(li!=null&&li.size()>0){
			m.put("msg","该用户名已存在，请换一个");
			m.put("status","0");
			return m;
		}
		m.put("msg","该用户名可用");
		m.put("status","1");
		return m;
	}


}
