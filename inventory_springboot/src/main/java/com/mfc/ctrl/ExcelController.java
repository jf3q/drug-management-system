package com.mfc.ctrl;


import com.mfc.entity.*;
import com.mfc.service.*;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

@Controller
@RequestMapping(value = "/jsp/excel")
public class ExcelController {

	@Autowired
	private InIngredientsService ingredientsService;
	@Autowired
	private InReceiptService receiptService;
	@Autowired
	private InDetailService detailService;
	@Autowired
	private InWreckService wreckService;


    //创建表头
    private void createTitle(HSSFWorkbook workbook,HSSFSheet sheet,String title[]){
        HSSFRow row = sheet.createRow(0);
        //设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        sheet.setColumnWidth(1,12*256);
        sheet.setColumnWidth(3,17*256);

        //设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setFont(font);

        HSSFCell cell;
        for(int i=0;i<title.length;i++){
        	 cell = row.createCell(i);
             cell.setCellValue(title[i]);
             cell.setCellStyle(style);
        }

    }
  //生成excel文件
    protected void buildExcelFile(String filename,HSSFWorkbook workbook) throws Exception{
        FileOutputStream fos = new FileOutputStream(filename);
        workbook.write(fos);
        fos.flush();
        fos.close();
    }

    //浏览器下载excel
    protected void buildExcelDocument(String filename,HSSFWorkbook workbook,HttpServletResponse response) throws Exception{
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode(filename, "utf-8"));
        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @RequestMapping("/getIngredients")
    public String getIngredients(InIngredients o,HttpServletResponse response) throws Exception{
    	String  title="库存表";


    	HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(title);
        String []titles={"编号","名称","分类","库存","单位","描述或者备注"};
        createTitle(workbook,sheet,titles);

        List<InIngredients> rows = ingredientsService.list(o);

        //设置日期格式
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        //新增数据行，并且设置单元格数据
        int rowNum=1;
        for(InIngredients g:rows){
            HSSFRow row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(g.getCode());
            row.createCell(1).setCellValue(g.getIname());
            row.createCell(2).setCellValue(g.getTname());
            row.createCell(3).setCellValue(g.getInventory());
            row.createCell(4).setCellValue(g.getUnit());
            row.createCell(5).setCellValue(g.getIdesc());



//            HSSFCell cell = row.createCell(10);
//            cell.setCellValue(stu.getBorth());
//            cell.setCellStyle(style);
            rowNum++;
        }

        String fileName = title+".xls";

        //生成excel文件
        buildExcelFile(fileName, workbook);

        //浏览器下载excel
        buildExcelDocument(fileName,workbook,response);

        return "download excel";
    }



    @RequestMapping("/getReceipt")
    public String getReceipt(InReceipt o,HttpServletResponse response) throws Exception{
    	String  title="采购表";

    	if(o.getRealname()!=null&&!o.getRealname().isEmpty()){
    		title+=o.getRealname();
    	}
    	HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(title);


        String []titles={"编号","名称","分类","需采购数量","采购备注","采购日期","采购员","供货商","状态","实际采买数量","单价","总价","实际采购时间","采购员备注","折损数量","折损备注","实际入库数量","入库时间","入库备注"};
        createTitle(workbook,sheet,titles);

        List<InReceipt> rows = receiptService.list(o);

        //设置日期格式
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        //新增数据行，并且设置单元格数据
        int rowNum=1;
        for(InReceipt g:rows){
            HSSFRow row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(g.getCode());
            row.createCell(1).setCellValue(g.getIname());
            row.createCell(2).setCellValue(g.getTname());
            row.createCell(3).setCellValue(g.getBuy_num()+g.getUnit());
            row.createCell(4).setCellValue(g.getBuy_note());
            row.createCell(5).setCellValue(g.getBuy_ts());
            row.createCell(6).setCellValue(g.getRealname());
            row.createCell(7).setCellValue(g.getSupplier_msg());
            String s="";
            if(g.getStatus().equals("0"))s="待采购";
            if(g.getStatus().equals("1"))s="已采购，待入库";
            if(g.getStatus().equals("2"))s="已入库";
            row.createCell(8).setCellValue(s);
            row.createCell(9).setCellValue(g.getBuyed_num()+g.getUnit());
            row.createCell(10).setCellValue(g.getPrice()+"元/"+g.getUnit());
            row.createCell(11).setCellValue(g.getTotal_money()+"元");
            row.createCell(12).setCellValue(g.getBuyed_ts());
            row.createCell(13).setCellValue(g.getBuyed_note());
            row.createCell(14).setCellValue(g.getWreck()+g.getUnit());
            row.createCell(15).setCellValue(g.getWreck_desc());
            row.createCell(16).setCellValue(g.getStorage_num()+g.getUnit());
            row.createCell(17).setCellValue(g.getStorage_ts());
            row.createCell(18).setCellValue(g.getStorage_note());



//            HSSFCell cell = row.createCell(10);
//            cell.setCellValue(stu.getBorth());
//            cell.setCellStyle(style);
            rowNum++;
        }

        String fileName = title+".xls";

        //生成excel文件
        buildExcelFile(fileName, workbook);

        //浏览器下载excel
        buildExcelDocument(fileName,workbook,response);

        return "download excel";
    }

    @RequestMapping("/getWreck")
    public String getWreck(InWreck o,HttpServletResponse response) throws Exception{
    	String  title="折损表";


    	HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(title);


        String []titles={"编号","名称","分类","折损数量","备注","创建日期"};
        createTitle(workbook,sheet,titles);

        List<InWreck> rows = wreckService.list(o);

        //设置日期格式
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        //新增数据行，并且设置单元格数据
        int rowNum=1;
        for(InWreck g:rows){
            HSSFRow row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(g.getCode());
            row.createCell(1).setCellValue(g.getIname());
            row.createCell(2).setCellValue(g.getTname());
            row.createCell(3).setCellValue(g.getWreck()+g.getUnit());
            row.createCell(4).setCellValue(g.getWreck_desc());
            row.createCell(5).setCellValue(g.getCts());




//            HSSFCell cell = row.createCell(10);
//            cell.setCellValue(stu.getBorth());
//            cell.setCellStyle(style);
            rowNum++;
        }

        String fileName = title+".xls";

        //生成excel文件
        buildExcelFile(fileName, workbook);

        //浏览器下载excel
        buildExcelDocument(fileName,workbook,response);

        return "download excel";
    }

    @Autowired
    InventoryInboundService inboundService;

    @RequestMapping("/getDetail")
    public String getDetail(InDetail o,HttpServletResponse response) throws Exception{
    	String  title="入库记录表";


    	HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(title);


        String []titles={"编号","药品名称","生产单位","数量","单价","总金额","原因","创建时间","更新时间","操作员名称"};
        createTitle(workbook,sheet,titles);

        List<InventoryInbound> rows = inboundService.list(new InventoryInbound().setIoType(1));

        //设置日期格式
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        //新增数据行，并且设置单元格数据
        int rowNum=1;
        for(InventoryInbound g:rows){
            HSSFRow row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(g.getId());
            row.createCell(1).setCellValue(g.getMedicineName());
            row.createCell(2).setCellValue(g.getManufacturer());
            row.createCell(3).setCellValue(g.getInboundQuantity());
            row.createCell(4).setCellValue(g.getInboundPrice());
            row.createCell(5).setCellValue(g.getTotalAmount());
            row.createCell(6).setCellValue(g.getInboundReason());
            row.createCell(7).setCellValue(g.getCreatedAt());
            row.createCell(8).setCellValue(g.getUpdatedAt());
            row.createCell(9).setCellValue(g.getUserName());




//            HSSFCell cell = row.createCell(10);
//            cell.setCellValue(stu.getBorth());
//            cell.setCellStyle(style);
            rowNum++;
        }

        String fileName = title+".xls";

        //生成excel文件
        buildExcelFile(fileName, workbook);

        //浏览器下载excel
        buildExcelDocument(fileName,workbook,response);

        return "download excel";
    }

}
