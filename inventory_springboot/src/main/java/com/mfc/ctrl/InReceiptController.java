package com.mfc.ctrl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mfc.cons.Sys;
import com.mfc.entity.*;
import com.mfc.service.*;
import com.mfc.untils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/jsp/receipt")
public class InReceiptController {

	@Autowired
	private InReceiptService receiptService;
	@Autowired
	private InTypeService typeService;
	@Autowired
	private InStaffService staffService;
	@Autowired
	private InIngredientsService ingredientsService;
	@Autowired
	private InDetailService detailService;
	@Autowired
	private InWreckService WreckService;

	@Autowired
	private InventoryInboundService inboundService;

	@Autowired
	private UserService userService;
	@Autowired
	private MedicineService medicineService;
	@Autowired
	private PatientService patientService;


	@RequestMapping("/page")
	public String page(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InReceipt o,Model model,HttpSession session) {
		InStaff l=(InStaff) session.getAttribute("staffU");
		if(l.getRole().equals(Sys.role_buyer))
			o.setBuyer_id(l.getId());
		System.out.println("l.getRole().equals(Sys.role_buyer)======="+l.getRole().equals(Sys.role_buyer));

		PageHelper.startPage(pageNo,Sys.pageSize," id desc ");
		List<InReceipt> li=receiptService.list(o);
		PageInfo<InReceipt> pageInfo=new PageInfo<InReceipt>(li, Sys.pageSize);

		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("o", o);

		List<InType> tli=typeService.list(null);
		model.addAttribute("tli", tli);

		return Sys.jsp+"/receipt_page";
	}


	@RequestMapping("/toedit")
	public String toedit(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InventoryInbound o,Model model,HttpSession session) {
		if(o.getId()!=null){
			o=inboundService.list(new InventoryInbound().setId(o.getId())).get(0);
			model.addAttribute("o", o);
		}
		model.addAttribute("pageNo", pageNo);

		List<User> sli = userService.getList(new User().setRoleType(1));
		model.addAttribute("sli", sli);


		List<Medicine> tli = medicineService.list(new Medicine());
		model.addAttribute("tli", tli);

		return Sys.jsp+"/receipt_add";
	}

	@RequestMapping("/save")
	public String save(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InventoryInbound o,Model model) {
		System.out.println(o);
		return "redirect:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
	}


	@RequestMapping("/del")
	public String del(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InReceipt o,Model model) {
		try {
			InReceipt old=receiptService.getById(o.getId());
			if(old.getStatus().equals("0")){
				receiptService.delete(o.getId());
			}else{
				model.addAttribute("msg", "该采购单状态异常，请勿重复操作");
				return "forward:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
			}
		} catch (Exception e) {
			model.addAttribute("msg", "请先删除该采购单关联的其他信息");
			return "forward:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
		}
		return "redirect:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
	}


	@RequestMapping("/buyed")
	public String buyed(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InReceipt o,Model model) {
		InReceipt old=receiptService.getById(o.getId());
		if(old.getStatus().equals("0")){
			o.setBuyed_ts(DateUtils.DateTimeToString(new Date()));
			o.setStatus("1");
			o.setTotal_money(o.getBuyed_num()*o.getPrice());
			receiptService.update(o);
			return "redirect:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
		}else{
			model.addAttribute("msg", "该采购单状态异常，请勿重复操作");
			return "forward:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
		}

	}

	@RequestMapping("/putInStorage")
	public String putInStorage(@RequestParam(value="pageNo",defaultValue="1")int pageNo
			,InReceipt o,Model model) {
		InReceipt old=receiptService.getById(o.getId());
		if(old.getStatus().equals("1")){
			o.setStorage_ts(DateUtils.DateTimeToString(new Date()));
			o.setStatus("2");
			o.setTotal_money(o.getBuyed_num()*o.getPrice());
			receiptService.update(o);

			InIngredients i=ingredientsService.getById(old.getIngredients_id());
			i.setInventory(i.getInventory()+o.getStorage_num());
			ingredientsService.update(i);


			InDetail d=new InDetail();
			d.setDdesc("采购入库");
			d.setIngredients_id(i.getId());
			d.setUnit(i.getUnit());
			d.setType("+");
			d.setInventory(o.getStorage_num());
			d.setFrom_id("+receipt_"+o.getId());
			detailService.insert(d);

			if(o.getWreck()>0){
				d.setDdesc("采购入库前的折损："+o.getWreck_desc());
				d.setIngredients_id(i.getId());
				d.setUnit(i.getUnit());
				d.setType("-");
				d.setInventory(o.getWreck());
				d.setFrom_id("-receipt_"+o.getId());
				detailService.insert(d);

				InWreck w=new InWreck();
				w.setWreck(o.getWreck());
				w.setWreck_desc("采购入库前的折损："+o.getWreck_desc());
				w.setIngredients_id(i.getId());
				w.setUnit(i.getUnit());
				WreckService.insert(w);
			}


			return "redirect:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
		}else{
			model.addAttribute("msg", "该采购单状态异常，请勿重复操作");
			return "forward:/"+Sys.jsp+"/receipt/page?pageNo="+pageNo;
		}

	}

}
