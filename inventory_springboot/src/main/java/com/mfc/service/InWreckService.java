package com.mfc.service;


import com.mfc.entity.InWreck;

import java.util.List;





public interface   InWreckService {
	 public InWreck getById(Integer  id);
	 public void insert(InWreck o);
	 public void update(InWreck o);
	 public List<InWreck> list(InWreck o);
	 public void delete(Integer id);

}
