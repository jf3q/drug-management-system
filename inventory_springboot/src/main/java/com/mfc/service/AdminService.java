package com.mfc.service;

import com.mfc.entity.Admin;
import com.mfc.entity.User;

public interface AdminService {
    Admin selectByUserId(Integer id);

    void delete(Integer id);

    void insert(User o);


    void update(User o);
}
