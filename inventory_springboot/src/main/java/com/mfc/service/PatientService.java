package com.mfc.service;

import com.mfc.entity.Patient;
import com.mfc.entity.User;

import java.util.List;

public interface PatientService {

    List<Patient> list(User o);

    Patient selectByUserId(Integer id);

    void insert(Patient o);


    void delete(Integer id);

    void update(Patient o);
}
