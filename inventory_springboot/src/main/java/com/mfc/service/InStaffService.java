package com.mfc.service;


import com.mfc.entity.InStaff;

import java.util.List;





public interface  InStaffService {
	 public InStaff getById(Integer  id);
	 public void insert(InStaff o);
	 public void update(InStaff o);
	 public List<InStaff> list(InStaff o);
	 public void delete(Integer id);

}
