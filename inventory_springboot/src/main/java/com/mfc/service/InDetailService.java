package com.mfc.service;


import com.mfc.entity.InDetail;

import java.util.List;





public interface    InDetailService {
	 public InDetail getById(Integer  id);
	 public void insert(InDetail o);
	 public void update(InDetail o);
	 public List<InDetail> list(InDetail o);
	 public void delete(Integer id);
	 public InDetail getByFromId(String  FromId);


}
