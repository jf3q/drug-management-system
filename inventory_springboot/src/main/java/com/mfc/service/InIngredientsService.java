package com.mfc.service;


import com.mfc.entity.InIngredients;

import java.util.List;





public interface  InIngredientsService {
	 public InIngredients getById(Integer  id);
	 public void insert(InIngredients o);
	 public void update(InIngredients o);
	 public List<InIngredients> list(InIngredients o);
	 public void delete(Integer id);

}
