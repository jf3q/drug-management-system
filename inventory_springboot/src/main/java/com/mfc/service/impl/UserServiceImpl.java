package com.mfc.service.impl;

import com.mfc.dao.UserMapper;
import com.mfc.dto.LoginDto;
import com.mfc.entity.User;
import com.mfc.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public List<User> list(LoginDto o) {
        // TODO Auto-generated method stub
        return userMapper.list(o);
    }

    @Override
    public void insert(User u) {
        userMapper.insert(u);
    }

    @Override
    public void delete(Integer userId) {
        userMapper.delete(userId);
    }

    @Override
    public List<User>  getList(User o) {
       return userMapper.getList(o);
    }

    @Override
    public void updatePass(User userVo) {
        userMapper.updatePass(userVo);
    }

    @Override
    public void updateAccount(User o) {
        userMapper.updateAccount(o);
    }

    @Override
    public User getById(Integer id) {
        return userMapper.getById(id);
    }

    @Override
    public List<User> getByUserAccountList(String account) {
        return userMapper.getByUserAccountList(account);
    }
}
