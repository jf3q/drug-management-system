package com.mfc.service.impl;

import com.mfc.dao.MedicineMapper;
import com.mfc.entity.Medicine;
import com.mfc.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("medicineService")
public class MedicineServiceImpl implements MedicineService {

    @Autowired
    MedicineMapper medicineMapper;

    @Override
    public List<Medicine> list(Medicine o) {
        return medicineMapper.list(o);
    }

    @Override
    public void update(Medicine o) {
        medicineMapper.update(o);
    }

    @Override
    public void insert(Medicine o) {
        medicineMapper.insert(o);
    }

    @Override
    public void delete(Integer medicineId) {
        medicineMapper.delete(medicineId);
    }

    @Override
    public void updateStockQuantity(Medicine medicine) {
        medicineMapper.updateStockQuantity(medicine);
    }


}
