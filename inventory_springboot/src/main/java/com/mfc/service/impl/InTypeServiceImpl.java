package com.mfc.service.impl;

import com.mfc.dao.InTypeMapper;
import com.mfc.entity.InType;
import com.mfc.service.InTypeService;
import com.mfc.untils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inTypeService")
public class    InTypeServiceImpl implements InTypeService{

	@Resource
	private InTypeMapper inTypeMapper;

	@Override
	public InType getById(Integer id) {
		return inTypeMapper.getById(id);
	}

	@Override
	public void insert(InType o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		inTypeMapper.insert(o);
	}

	@Override
	public void update(InType o) {
		// TODO Auto-generated method stub
		inTypeMapper.update(o);
	}

	@Override
	public List<InType> list(InType o) {
		// TODO Auto-generated method stub
		return inTypeMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inTypeMapper.delete(id);
	}






}
