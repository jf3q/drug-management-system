package com.mfc.service.impl;

import com.mfc.cons.Sys;
import com.mfc.dao.InStaffMapper;
import com.mfc.entity.InStaff;
import com.mfc.service.InStaffService;
import com.mfc.untils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inStaffService")
public class   InStaffServiceImpl implements InStaffService{

	@Resource
	private InStaffMapper inStaffMapper;

	@Override
	public InStaff getById(Integer id) {
		return inStaffMapper.getById(id);
	}

	@Override
	public void insert(InStaff o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		o.setStatus("1");
		o.setUpass(Sys.default_pwd);
		inStaffMapper.insert(o);
	}

	@Override
	public void update(InStaff o) {
		// TODO Auto-generated method stub
		inStaffMapper.update(o);
	}

	@Override
	public List<InStaff> list(InStaff o) {
		// TODO Auto-generated method stub
		return inStaffMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inStaffMapper.delete(id);
	}






}
