package com.mfc.service.impl;

import com.mfc.dao.PatientMapper;
import com.mfc.entity.Patient;
import com.mfc.entity.User;
import com.mfc.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("patientService")
public class PatientServiceImpl implements PatientService {
    @Autowired
    PatientMapper patientMapper;

    @Override
    public List<Patient> list(User o) {
        return patientMapper.list(o);
    }

    @Override
    public Patient selectByUserId(Integer id) {
        return patientMapper.getById(id);
    }

    @Override
    public void insert(Patient o) {
        patientMapper.insert(o);
    }

    @Override
    public void delete(Integer id) {
        patientMapper.delete(id);
    }

    @Override
    public void update(Patient o) {
        patientMapper.update(o);
    }
}
