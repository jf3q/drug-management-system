package com.mfc.service.impl;

import com.mfc.dao.InIngredientsMapper;
import com.mfc.entity.InIngredients;
import com.mfc.service.InIngredientsService;
import com.mfc.untils.DateUtils;
import com.mfc.untils.RandomUntils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inIngredientsService")
public class   InIngredientsServiceImpl implements InIngredientsService{

	@Resource
	private InIngredientsMapper inIngredientsMapper;

	@Override
	public InIngredients getById(Integer id) {
		return inIngredientsMapper.getById(id);
	}

	@Override
	public void insert(InIngredients o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		o.setCode(RandomUntils.getCode());
		inIngredientsMapper.insert(o);
	}

	@Override
	public void update(InIngredients o) {
		// TODO Auto-generated method stub
		inIngredientsMapper.update(o);
	}

	@Override
	public List<InIngredients> list(InIngredients o) {
		// TODO Auto-generated method stub
		return inIngredientsMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inIngredientsMapper.delete(id);
	}






}
