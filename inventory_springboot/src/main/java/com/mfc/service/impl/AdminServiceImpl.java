package com.mfc.service.impl;

import com.mfc.dao.AdminMapper;
import com.mfc.entity.Admin;
import com.mfc.entity.User;
import com.mfc.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Resource
    AdminMapper adminMapper;

    @Override
    public Admin selectByUserId(Integer id) {
        return adminMapper.selectByUserId(id);
    }

    @Override
    public void delete(Integer id) {
        adminMapper.delete(id);
    }

    @Override
    public void insert(User o) {
        adminMapper.insert(o);
    }

    @Override
    public void update(User o) {
        adminMapper.update(o);
    }
}
