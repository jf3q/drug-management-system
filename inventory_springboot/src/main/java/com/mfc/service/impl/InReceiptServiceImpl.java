package com.mfc.service.impl;

import com.mfc.dao.InReceiptMapper;
import com.mfc.entity.InReceipt;
import com.mfc.service.InReceiptService;
import com.mfc.untils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inReceiptService")
public class  InReceiptServiceImpl implements InReceiptService{

	@Resource
	private InReceiptMapper inReceiptMapper;

	@Override
	public InReceipt getById(Integer id) {
		return inReceiptMapper.getById(id);
	}

	@Override
	public void insert(InReceipt o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		o.setStatus("0");
		inReceiptMapper.insert(o);
	}

	@Override
	public void update(InReceipt o) {
		// TODO Auto-generated method stub
		inReceiptMapper.update(o);
	}

	@Override
	public List<InReceipt> list(InReceipt o) {
		// TODO Auto-generated method stub
		return inReceiptMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inReceiptMapper.delete(id);
	}






}
