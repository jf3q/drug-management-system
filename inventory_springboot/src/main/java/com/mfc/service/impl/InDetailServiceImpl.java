package com.mfc.service.impl;

import com.mfc.dao.InDetailMapper;
import com.mfc.entity.InDetail;
import com.mfc.service.InDetailService;
import com.mfc.untils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inDetailService")
public class    InDetailServiceImpl implements InDetailService{

	@Resource
	private InDetailMapper inDetailMapper;

	@Override
	public InDetail getById(Integer id) {
		return inDetailMapper.getById(id);
	}

	@Override
	public void insert(InDetail o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		inDetailMapper.insert(o);
	}

	@Override
	public void update(InDetail o) {
		// TODO Auto-generated method stub
		inDetailMapper.update(o);
	}

	@Override
	public List<InDetail> list(InDetail o) {
		// TODO Auto-generated method stub
		return inDetailMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inDetailMapper.delete(id);
	}

	@Override
	public InDetail getByFromId(String FromId) {
		InDetail o=new InDetail();
		o.setFrom_id(FromId);
		List<InDetail> li=inDetailMapper.list(o);
		if(li!=null&&li.size()>0)return li.get(0);
		return null;
	}






}
