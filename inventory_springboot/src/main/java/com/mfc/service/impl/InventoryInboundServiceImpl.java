package com.mfc.service.impl;

import com.mfc.dao.InventoryInboundMapper;
import com.mfc.entity.InventoryInbound;
import com.mfc.service.InventoryInboundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("inventoryInboundService")
public class InventoryInboundServiceImpl implements InventoryInboundService {
    @Autowired
    InventoryInboundMapper inboundMapper;

    @Override
    public List<InventoryInbound> list(InventoryInbound o) {
        return inboundMapper.list(o);
    }

    @Override
    public void delete(Integer id) {
        inboundMapper.delete(id);
    }

    @Override
    public void insert(InventoryInbound o) {
        inboundMapper.insert(o);
    }

    @Override
    public void updateIoType(InventoryInbound o) {
        inboundMapper.updateIoType(o);
    }
}
