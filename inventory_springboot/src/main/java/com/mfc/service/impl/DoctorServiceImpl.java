package com.mfc.service.impl;

import com.mfc.dao.DoctorMapper;
import com.mfc.entity.Doctor;
import com.mfc.entity.User;
import com.mfc.service.DoctorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("doctorService")
public class DoctorServiceImpl implements DoctorService {
    @Resource
    DoctorMapper doctorMapper;

    @Override
    public Doctor selectByUserId(Integer id) {
        return doctorMapper.selectByUserId(id);
    }

    @Override
    public List<Doctor> list(User o) {
        return doctorMapper.list(o);
    }

    @Override
    public void delete(Integer id) {
        doctorMapper.delete(id);
    }

    @Override
    public void insert(User o) {
        doctorMapper.insert(o);
    }

    @Override
    public void update(User o) {
        doctorMapper.update(o);
    }
}
