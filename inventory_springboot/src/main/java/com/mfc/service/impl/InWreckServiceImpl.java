package com.mfc.service.impl;

import com.mfc.dao.InWreckMapper;
import com.mfc.entity.InWreck;
import com.mfc.service.InWreckService;
import com.mfc.untils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inWreckService")
public class   InWreckServiceImpl implements InWreckService{

	@Resource
	private InWreckMapper inWreckMapper;

	@Override
	public InWreck getById(Integer id) {
		return inWreckMapper.getById(id);
	}

	@Override
	public void insert(InWreck o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		inWreckMapper.insert(o);
	}

	@Override
	public void update(InWreck o) {
		// TODO Auto-generated method stub
		inWreckMapper.update(o);
	}

	@Override
	public List<InWreck> list(InWreck o) {
		// TODO Auto-generated method stub
		return inWreckMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inWreckMapper.delete(id);
	}






}
