package com.mfc.service.impl;

import com.mfc.dao.InSupplierMapper;
import com.mfc.entity.InSupplier;
import com.mfc.service.InSupplierService;
import com.mfc.untils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;







/**
 *  @author jf3q.com
 *
 */
@Service("inSupplierService")
public class    InSupplierServiceImpl implements InSupplierService{

	@Resource
	private InSupplierMapper inSupplierMapper;

	@Override
	public InSupplier getById(Integer id) {
		return inSupplierMapper.getById(id);
	}

	@Override
	public void insert(InSupplier o) {
		o.setCts(DateUtils.DateTimeToString(new Date()));
		inSupplierMapper.insert(o);
	}

	@Override
	public void update(InSupplier o) {
		// TODO Auto-generated method stub
		inSupplierMapper.update(o);
	}

	@Override
	public List<InSupplier> list(InSupplier o) {
		// TODO Auto-generated method stub
		return inSupplierMapper.list(o);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		inSupplierMapper.delete(id);
	}






}
