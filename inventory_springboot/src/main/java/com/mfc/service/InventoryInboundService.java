package com.mfc.service;


import com.mfc.entity.InventoryInbound;

import java.util.List;

public interface InventoryInboundService {
    List<InventoryInbound> list(InventoryInbound o);

    void delete(Integer id);

    void insert(InventoryInbound o);

    void updateIoType(InventoryInbound o);
}
