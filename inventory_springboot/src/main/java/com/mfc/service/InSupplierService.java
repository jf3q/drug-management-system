package com.mfc.service;


import com.mfc.entity.InSupplier;

import java.util.List;





public interface  InSupplierService {
	 public InSupplier getById(Integer  id);
	 public void insert(InSupplier o);
	 public void update(InSupplier o);
	 public List<InSupplier> list(InSupplier o);
	 public void delete(Integer id);

}
