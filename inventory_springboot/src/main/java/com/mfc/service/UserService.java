package com.mfc.service;

import com.mfc.dto.LoginDto;
import com.mfc.entity.User;

import java.util.List;

public interface UserService {

    public List<User> list(LoginDto o);


    void insert(User u);

    void delete(Integer userId);

    List<User>  getList(User o);

    void updatePass(User userVo);

    void updateAccount(User o);

    User getById(Integer id);

    List<User> getByUserAccountList(String account);
}
