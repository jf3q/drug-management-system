package com.mfc.service;


import com.mfc.entity.InType;

import java.util.List;





public interface  InTypeService {
	 public InType getById(Integer  id);
	 public void insert(InType o);
	 public void update(InType o);
	 public List<InType> list(InType o);
	 public void delete(Integer id);

}
