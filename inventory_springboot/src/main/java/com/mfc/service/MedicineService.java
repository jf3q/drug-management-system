package com.mfc.service;

import com.mfc.entity.Medicine;

import java.util.List;

public interface MedicineService {
    List<Medicine> list(Medicine o);

    void update(Medicine o);

    void insert(Medicine o);

    void delete(Integer medicineId);

    void updateStockQuantity(Medicine medicine);
}
