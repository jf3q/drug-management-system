package com.mfc.service;


import com.mfc.entity.InReceipt;

import java.util.List;





public interface  InReceiptService {
	 public InReceipt getById(Integer  id);
	 public void insert(InReceipt o);
	 public void update(InReceipt o);
	 public List<InReceipt> list(InReceipt o);
	 public void delete(Integer id);

}
