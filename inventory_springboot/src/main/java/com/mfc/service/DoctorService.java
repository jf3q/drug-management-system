package com.mfc.service;

import com.mfc.entity.Doctor;
import com.mfc.entity.User;

import java.util.List;

public interface DoctorService {
    Doctor selectByUserId(Integer id);

    List<Doctor> list(User o);

    void delete(Integer id);

    void insert(User o);

    void update(User o);
}
